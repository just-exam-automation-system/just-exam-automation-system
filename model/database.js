const { Sequelize, DataTypes } = require('sequelize')

const sequelize = new Sequelize('samiul', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    pool: { max: 5, min: 0, idle: 10000 }
})

sequelize.authenticate()
    .then(() => {
        console.log('databse connect success')
    })
    .catch(error => {
        console.log('error ' + error)
    })

const db = {}
db.Sequelize = Sequelize
db.sequelize = sequelize

db.sequelize.sync({ force: false })
    .then(() => {
        console.log('sync databse')
    })
    .catch(e => {
        console.log(e)
    })

// db.sequelize.sync({ force: false })
//     .then(() => {
//         console.log('sync databse')
//     })

// db.sequelize.sync({ force: false }).then(() => {
//     console.log("sync database")
// }).catch((err) => {
//     console.log(err)
// })


db.users = require('./users')(sequelize, DataTypes)
db.question_informations = require('./question_infromations')(sequelize, DataTypes)
db.questions = require('./questions')(sequelize, DataTypes)
db.courses = require('./course')(sequelize, DataTypes);
db.sessions = require('./session')(sequelize, DataTypes);
db.exam_committee_roles = require('./exam_committee_role')(sequelize, DataTypes)
db.exam_committee = require('./exam_committee')(sequelize, DataTypes)
db.exam_committee_members = require('./exam_committee_members')(sequelize, DataTypes)
db.course_instructor_panel = require('./course_instructor')(sequelize, DataTypes)
db.course_instructor_panel_member = require('./course_instructor_panel_member')(sequelize, DataTypes)
db.course_examiner_panel = require('./course_examiner_panel')(sequelize, DataTypes)
db.course_examiner_panel_member = require('./course_examiner_panel_member')(sequelize, DataTypes)
db.results = require('./result')(sequelize, DataTypes)
db.students = require('./students')(sequelize, DataTypes)

// one to many relation between sessions table and course table
db.sessions.hasMany(db.courses, { foreignKey: 'session_name' })
db.courses.belongsTo(db.sessions, { foreignKey: 'session_name' })
    // one to many relation between question_informations table and questions table
db.question_informations.hasMany(db.questions, { foreignKey: 'question_information_id' });
db.questions.belongsTo(db.question_informations, { foreignKey: 'question_information_id' })

// course instructor table 
db.course_instructor_panel.hasMany(db.course_instructor_panel_member, { foreignKey: 'course_instructor_panel_id' });
db.course_instructor_panel_member.belongsTo(db.course_instructor_panel, { foreignKey: 'course_instructor_panel_id' });
db.course_instructor_panel_member.belongsTo(db.users, { as: 'internal_instructor', foreignKey: 'internal' })
db.course_instructor_panel_member.belongsTo(db.users, { as: 'external_instructor', foreignKey: 'external' })
db.course_instructor_panel_member.belongsTo(db.courses, { foreignKey: 'course_id' })


// many to many relation between users, exam committee role, exam committee, exam committee members
db.exam_committee.hasMany(db.exam_committee_members, { foreignKey: 'exam_committee_id' })
db.exam_committee_members.belongsTo(db.exam_committee, { foreignKey: 'exam_committee_id' })
db.exam_committee_members.belongsTo(db.users, { foreignKey: 'member_id' })
db.exam_committee_members.belongsTo(db.exam_committee_roles, { foreignKey: 'member_role' })

//relations between course examiner panel and course examiner panel member
db.course_examiner_panel.hasMany(db.course_examiner_panel_member, { foreignKey: 'course_examiner_panel_id' });
db.course_examiner_panel_member.belongsTo(db.course_examiner_panel, { foreignKey: 'course_examiner_panel_id' });
db.course_examiner_panel_member.belongsTo(db.users, { as: 'internal_examiner', foreignKey: 'internal' })
db.course_examiner_panel_member.belongsTo(db.users, { as: 'external_examiner', foreignKey: 'external' })
db.course_examiner_panel_member.belongsTo(db.users, { as: 'third_examiner', foreignKey: 'third' })
db.course_examiner_panel_member.belongsTo(db.courses, { foreignKey: 'course_id' })


//relations between result ,course examiner panel and question information table
db.question_informations.hasMany(db.results, { foreignKey: 'question_information_id' });
db.results.belongsTo(db.question_informations, { foreignKey: 'question_information_id' });
db.results.belongsTo(db.course_examiner_panel, { foreignKey: 'course_examiner_panel_id' });


//relations between exam committee,course instructor panel member, course and users table

db.exam_committee.hasMany(db.students, { foreignKey: 'exam_committee_id' });
db.students.belongsTo(db.exam_committee, { foreignKey: 'exam_committee_id' });
db.students.belongsTo(db.course_instructor_panel_member, { foreignKey: 'course_instructor_panel_member_id' });
db.students.belongsTo(db.courses, { foreignKey: 'course_id' });



module.exports = db