module.exports = (sequelize, DataTypes) => {
    const Results = sequelize.define('results', {
        question_information_id: {
            type: DataTypes.INTEGER
        },
        course_examiner_panel_id:{
            type:DataTypes.INTEGER
        },
        student_id: {
            type: DataTypes.INTEGER
        },
        internal_total: {
            type: DataTypes.STRING
        },
        internal_detail: {
            type: DataTypes.STRING
        },
        external_total: {
            type: DataTypes.STRING
        },
        external_detail: {
            type: DataTypes.STRING
        },
        third_total:{
            type: DataTypes.STRING
        },
        third_detail:{
            type: DataTypes.STRING
        },
        ce: {
            type: DataTypes.DOUBLE
        },
        attendence: {
            type: DataTypes.DOUBLE
        },
        internal_last_edit:{
            type:DataTypes.DATE
        },
        external_last_edit:{
            type:DataTypes.DATE
        },
        third_last_edit:{
            type:DataTypes.DATE
        }
    }, {
        timestamps: true
    })
    return Results
}