module.exports = (sequelize, DataTypes) => {
    const Course_Instructor_Panel = sequelize.define('course_instructor_panels', {
        exam_committee_id: {
            type: DataTypes.INTEGER
        },
        department_name: {
            type: DataTypes.STRING
        },
        session: {
            type: DataTypes.STRING
        },
        year: {
            type: DataTypes.STRING
        },
        semester: {
            type: DataTypes.STRING
        },
        status: {
            type: DataTypes.STRING
        }
    }, {
        timestamps: true
    })
    return Course_Instructor_Panel
}

