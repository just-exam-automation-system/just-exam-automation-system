module.exports = (sequelize, DataTypes) => {
    const Course_Examiner_Panel_Member = sequelize.define("course_examiner_panel_members", {
        course_examiner_panel_id: {
            type: DataTypes.INTEGER
        },
        course_id: {
            type: DataTypes.STRING
        },
        internal: {
            type: DataTypes.STRING
        },
        external: {
            type: DataTypes.STRING
        },
        third:{
            type:DataTypes.STRING
        },
        uuid: {
            type: DataTypes.STRING
        },
        uuid_expire_date: {
            type: DataTypes.STRING
        },
        status: {
            type: DataTypes.STRING,
        }
    }, {
        timestamps: true
    })
    return Course_Examiner_Panel_Member

}