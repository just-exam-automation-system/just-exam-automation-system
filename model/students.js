module.exports = (sequelize, DataTypes) => {
    const Students = sequelize.define('students', {
        department_name: {
            type: DataTypes.STRING
        },
        exam_committee_id: {
            type: DataTypes.INTEGER
        },
        course_instructor_panel_member_id:{
            type:DataTypes.INTEGER
        },
        course_id:{
            type:DataTypes.INTEGER
        },
        student_id: {
            type: DataTypes.INTEGER
        }
    }, {
        timestamps: true
    })
    return Students
}