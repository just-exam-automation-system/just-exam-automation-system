const Flash=require('../utils/Flash')

exports.getHomePage = async(req, res, next) => {
    try {
        console.log(req.user)
        if (req.user.role == '4') {
            return res.render('pages/student/student_homepage', { title: "Homepage " + req.user.student_id, user: req.user,flashMessage:Flash.getMessage(req) })
        } else if (req.user.role == '2' || req.user.role == '3') {
            return res.render('pages/teacher/teacher_homepage', { title: "Homepage-Teacher", user: req.user,flashMessage:Flash.getMessage(req) })
        } else if (req.user.role == '1') {
            return res.render('pages/admin/admin_homepage', { title: 'Homepage-Admin', department_name: req.user.department_name, user: req.user,flashMessage:Flash.getMessage(req) })
        }
    } catch (e) {
        console.log(e)
    }

}