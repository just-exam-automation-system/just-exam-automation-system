const db = require('../../model/database')

const Course = db.courses
const Exam_Committee_Roles = db.exam_committee_roles
const Users = db.users
const readXlsxFile = require("read-excel-file/node")
const course = require('../../model/course')

const Session = db.sessions
const Exam_Committee_Members = db.exam_committee_members
const Exam_Committees = db.exam_committee
const Course_Instructor_Panel = db.course_instructor_panel
const Course_Instructor_Panel_Member = db.course_instructor_panel_member
const Course_Examiner_Panel = db.course_examiner_panel
const Course_Examiner_Panel_Member = db.course_examiner_panel_member
const Student = db.students
const Question_Informations = db.question_informations

const { Op } = require("sequelize");
const course_instructor_panel_member = require('../../model/course_instructor_panel_member')
const { courses } = require('../../model/database')
const Flash = require('../../utils/Flash')
const exam_committee_role = require('../../model/exam_committee_role')


//get course create by file and manually page
exports.getCourseHomePage = async(req, res, next) => {
    try {
        await Session.findAll({
                order: [
                    ['id', 'ASC']
                ],
                attributes: ['id', 'session']
            })
            .then(sessions => {
                res.render('pages/admin/coursePage', { sessions, title: "Add Course Page", user: req.user, flashMessage: Flash.getMessage(req) })
            })
            .catch(e => {
                console.log(e)
                res.json({
                    message: 'Error occurred'
                })
            })
    } catch (e) {
        console.log(e)
    }

}

/** Semester Course list edit  
 * @version 14-8-22
 * @since 14-8-22
 * 
 * @param 
 *  $GET= []
 * 
 */

exports.getCourgeAddFilePageController = async(req, res, next) => {
    try {
        await Session.findAll({
                order: [
                    ['id', 'ASC']
                ],
                attributes: ['id', 'session']
            })
            .then(sessions => {
                res.render('pages/admin/coursefile', { sessions, title: "Add Course Page", user: req.user, flashMessage: Flash.getMessage(req) })
            })
            .catch(e => {
                console.log(e)
                res.json({
                    message: 'Error occurred'
                })
            })
    } catch (e) {
        console.log(e)
    }
}

//post create course by file
exports.postCourseFilePage = async(req, res, next) => {
    try {
        if (req.file == undefined) {
            req.flash('fail', 'Enter Course Excel FILE')
        } else {
            let path = 'public/uploads/' + req.file.filename
            let department_name = req.body.department
            let session = req.body.session;
            readXlsxFile(path).then((rows) => {
                rows.shift();
                let courses = []
                rows.forEach((row) => {
                    let course = {
                        department_name: department_name,
                        session_name: session,
                        course_code: row[0],
                        course_title: row[1],
                        course_type: row[2],
                        theory_credit: row[3],
                        lab_credit: row[4],
                        total_credit: row[5],
                        ce: row[6],
                        attendence: row[7],
                        see: row[8],
                        se: row[9],
                        total_mark: row[10]

                    }
                    courses.push(course)

                })
                Session.findAll({
                        order: [
                            ['id', 'ASC']
                        ],
                        attributes: ['id', 'session']
                    })
                    .then(sessions => {
                        res.render('pages/admin/createCourse', { course: courses, sessions, title: "Add Courses", user: req.user, flashMessage: Flash.getMessage(req) })
                    })
                    .catch(e => {
                        console.log(e)
                        res.json({
                            message: 'Error occurred'
                        })
                    })
            })
        }

    } catch (e) {
        console.log(e)
        res.send({
            message: "file upload fail" + req.file.originalname
        })
    }

}

//get manually create course page
exports.getCoursePage = async(req, res, next) => {
    return res.render('pages/admin/coursePage', { course: {}, title: "Add Courses", user: req.user, flashMessage: Flash.getMessage(req) })
}

exports.submitCourse = async(req, res, next) => {
    try {
        let courses = [];
        let course = {};

        for (let i = 0; i < req.body.course_code.length; i++) {
            course['department_name'] = req.body.department
            course['session_name'] = req.body.session
            course['course_code'] = req.body.course_code[i]
            course['course_title'] = req.body.course_title[i]
            course['course_type'] = req.body.course_type[i]
            course['theory_credit'] = req.body.theory_credit[i]
            course['lab_credit'] = req.body.lab_credit[i]
            course['total_credit'] = req.body.total_credit[i]
            course['ce'] = req.body.continuous_evaluation[i]
            course['attendence'] = req.body.attendence[i]
            course['see'] = req.body.see[i]
            course['se'] = req.body.se[i]
            course['total_mark'] = req.body.total_mark[i]


            courses.push(course)
            course = {}
        }

        await Course.bulkCreate(courses, {
                ignoreDuplicates: true
            }).then(() => {
                req.flash('success', 'Course Added Successfully')
                res.redirect('/')
            })
            .catch(() => {
                req.flash('fail', 'Course cannot be added')
            })
    } catch (e) {
        console.log(e)
    }
}

//get all uploaed course page
exports.viewAllCourses = async(req, res) => {
    try {
        const sessions = await Session.findAll()
        return res.render('pages/admin/viewcourse', { title: 'All Course', user: req.user, sessions, flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }
}

//get different session page 

exports.viewAllCoursesDetailsPageInSession = async(req, res) => {
    try {
        let department_name = req.user.department_name
        let session_name = req.params.id
        const sessions = await Session.findAll()
        const courses = await Course.findAll({
            where: {
                session_name: session_name,
                department_name: department_name
            },
            order: [
                ['id', 'ASC']
            ],
            attributes: ['id', 'department_name', 'session_name', 'course_code', 'course_title', 'course_type', 'theory_credit', 'lab_credit', 'total_credit', 'ce', 'attendence', 'see', 'se', 'total_mark']
        })
        return res.render('pages/admin/viewCoursesInSession', { courses, title: "All Courses", user: req.user, session_name, sessions, flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }
}


/** Semester Course list edit  
 * @version 10-8-22
 * @since 10-8-22
 * 
 * @param 
 *  $GET= [$session, $semester]
 * 
 */
exports.getEditSemesterCoursePageController = async(req, res, next) => {
    try {
        let department_name = req.user.department_name
        let session_name = req.params.id
        let semester_name = req.params.sem
        const courses = await Course.findAll({
            where: {
                session_name: session_name,
                department_name: department_name,
                course_code: {
                    [Op.like]: `%${semester_name}__%`
                }
            },
            order: [
                ['id', 'ASC']
            ],
            attributes: ['id', 'department_name', 'session_name', 'course_code', 'course_title', 'course_type', 'theory_credit', 'lab_credit', 'total_credit', 'ce', 'attendence', 'see', 'se', 'total_mark']
        });

        return res.render('pages/admin/editcourse', { courses, title: "All Courses", user: req.user, session_name, flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }
}

/** Semester Course list edit  
 * @version 10-8-22
 * @since 10-8-22
 * 
 * @param 
 *  $POST= [$session, $semester]
 * 
 */
exports.postEditSemesterCoursePageController = async(req, res, next) => {
    try {
        let department_name = req.user.department_name;
        let session_name = req.params.id;
        let semester_name = req.params.sem;
        const courses = await Course.findAll({
            where: {
                session_name: session_name,
                department_name: department_name,
                course_code: {
                    [Op.like]: `%${semester_name}__%`
                }
            },
            order: [
                ['id', 'ASC']
            ],
            attributes: ['id', 'department_name', 'session_name', 'course_code', 'course_title', 'course_type', 'theory_credit', 'lab_credit', 'total_credit', 'ce', 'attendence', 'see', 'se', 'total_mark']
        });
        let course_id = [];
        for (let i = 0; i < req.body.id.length; i++) {
            course_id.push(req.body.id[i])
        }

        let semester_courses = [];
        let course = {};
        for (let i = 0; i < req.body.course_code.length; i++) {
            course['id'] = course_id[i];
            course['department_name'] = req.body.department;
            course['session_name'] = req.body.session;
            course['course_code'] = req.body.course_code[i];
            course['course_title'] = req.body.course_title[i]
            course['course_type'] = req.body.course_type[i]
            course['theory_credit'] = req.body.theory_credit[i]
            course['lab_credit'] = req.body.lab_credit[i]
            course['total_credit'] = req.body.total_credit[i]
            course['ce'] = req.body.continuous_evaluation[i]
            course['attendence'] = req.body.attendence[i]
            course['see'] = req.body.see[i]
            course['se'] = req.body.se[i]
            course['total_mark'] = req.body.total_mark[i]
            semester_courses.push(course)
            course = {}
        }
        await Course.bulkCreate(semester_courses, {
            updateOnDuplicate: ['department_name', 'session_name', 'course_code', 'course_title', 'course_type', 'theory_credit', 'lab_credit', 'total_credit', 'ce', 'attendence', 'see', 'se', 'total_mark'],
            returning: true
        }, {
            where: {
                id: course_id
            }
        }).then(() => {
            req.flash('success', 'Course Updated Successfully')
            res.redirect('/admin/view-courses')
        }).catch(() => {
            req.flash('fail', 'Course Update Failed')
            res.redirect('/admin/view-courses')
        })

        return
    } catch (e) {
        console.log(e)
    }
}



//delete course page

exports.deleteCourse = (req, res) => {
    let { id } = req.params

    Course.destroy({
            where: {
                id: id
            }
        })
        .then(() => {
            Course.findAll()
                .then(() => {
                    res.redirect('/admin/all_courses')
                })
        })
        .catch(e => {
            console.log(e)
            res.json({
                message: 'Error occured'
            })
        })
}

// get create committee page 

exports.getCreateExamCommittee = async(req, res, next) => {
    // get all committee type 
    try {
        const committee_roles = await Exam_Committee_Roles.findAll()
        const sessions = await Session.findAll()
        const users = await Users.findAll({
            where: {
                role: '2'
            }
        })

        return res.render('pages/admin/createExamCommitteePage', { title: 'Exam Committee', committee_roles, users, user: req.user, sessions, flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }
}

// post create committee
exports.postCreateExamCommittee = async(req, res, next) => {
    try {
        let department = req.body.department
        let session = req.body.session
        let year = req.body.year
        let semester = req.body.semester

        const created_committee = await Exam_Committees.create({
            department_name: department,
            session: session,
            year: year,
            semester: semester,
            status: '1'
        })

        if (created_committee) {
            let committee_members = []
            let current_members;
            for (let i = 0; i < req.body.exam_committee_roles.length; i++) {
                current_members = {}
                current_members['member_id'] = req.body.exam_committee_members[i]
                current_members['member_role'] = req.body.exam_committee_roles[i]
                current_members['exam_committee_id'] = created_committee.id
                current_members['status'] = '1'
                committee_members.push(current_members)
            }
            const created_members = await Exam_Committee_Members.bulkCreate(committee_members, {
                returning: true
            }).then(() => {
                req.flash('success', 'Committee Created Successfully')
                return res.redirect('/')
            }).catch(() => {
                return req.flash('fail', 'Committee Create Failed')
            })
        } else {
            req.flash('fail', 'Committee Create Failed')
            return res.redirect('/')
        }

    } catch (e) {
        console.log(e)
    }
}

// create api for get exam committee roles

exports.getExamCommitteeRoles = async(req, res, next) => {
    try {
        const exam_committee_roles = await Exam_Committee_Roles.findAll()
        res.json(exam_committee_roles)
    } catch (e) {
        console.log(e)
    }
}

//// create api for get and users(teachers)

exports.getExternalTeachers = async(req, res, next) => {
    try {
        const external = await Users.findAll({
            where: {
                [Op.and]: [
                    { role: '2' },
                    // {
                    //     email: {
                    //         [Op.notLike]: '%@just%'
                    //     }
                    // }

                ]
            }
        })

        res.json(external)
    } catch (e) {
        console.log(e)
    }
}

exports.getInternalTeachers = async(req, res, next) => {
    try {
        const internal = await Users.findAll({
            where: {
                [Op.and]: [
                    { role: '2' },
                    // {
                    //     email: {
                    //         [Op.like]: '%@just%'
                    //     }
                    // }
                ]
            }
        })
        res.json(internal)
    } catch (e) {
        console.log(e)
    }
}

exports.getTeachers = async(req, res, next) => {
    try {
        const teachers = await Users.findAll({
            where: {
                role: '2'
            }
        })

        res.json(teachers)
    } catch (e) {
        console.log(e)
    }
}

// create api to get all courses 

exports.getAllCourses = async(req, res, next) => {
    try {
        const department = req.params.department;
        const session = req.params.session;
        const year = req.params.year;
        const semester = req.params.semester;
        const course = await Course.findAll({
            where: {
                department_name: department,
                session_name: session,
                [Op.or]: [{
                        course_code: {
                            [Op.like]: `% ${year}${semester}%`
                        }
                    },
                    {
                        course_code: {
                            [Op.like]: `%-${year}${semester}%`
                        }
                    }
                ]
            }
        })
        console.log("courses", course)
        res.json(course)
    } catch (e) {
        console.log(e)
    }
}


// view exam committee page 
exports.viewExamCommitteePage = async(req, res, next) => {
    try {
        const exam_committee = await Exam_Committees.findAll({
            where: {
                department_name: req.user.department_name,
                status: '1'
            }
        })
        return res.render('pages/admin/viewExamCommittee', { title: 'Exam Committee', user: req.user, exam_committee, flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }
}

// view exam committee details

exports.viewExamCommitteePageDetailsPage = async(req, res, next) => {
    try {
        let exam_committee_id = req.params.id
        const data = await Exam_Committees.findByPk(exam_committee_id, {
            include: [{
                model: Exam_Committee_Members,
                where: { status: '1' },
                include: [{ model: Users }, { model: Exam_Committee_Roles }]
            }]
        })
        let course_instructor_panel = await Course_Instructor_Panel.findAll({
            where: {
                exam_committee_id: exam_committee_id,
                status: '1'

            }
        })
        let course_examiner_panel = await Course_Examiner_Panel.findAll({
            where: {
                exam_committee_id: exam_committee_id,
                status: '1'
            }
        })
        if (course_instructor_panel.length === 0) {

            if (course_examiner_panel.length === 0) {
                return res.render("pages/admin/viewExamCommitteeEmptyInstructorAndExaminerPanel", { title: 'Exam Committee Details', user: req.user, data, course_instructor_panel, course_examiner_panel, flashMessage: Flash.getMessage(req) })
            } else {
                const course_examiner_panel_id = course_examiner_panel[0].id;
                const course_examiner_panel_member = await Course_Examiner_Panel.findByPk(course_examiner_panel_id, {
                    include: [{
                        model: Course_Examiner_Panel_Member,
                        where: { status: '1' },
                        include: [{ model: Users, as: 'internal_examiner' }, { model: Users, as: 'external_examiner' }, { model: Users, as: 'third_examiner' }, { model: Course }]
                    }]
                })

                return res.render('pages/admin/viewExamCommitteExaminerPanelWithEmptyInstructor', { title: 'Exam Committee Details', user: req.user, data, course_instructor_panel, course_examiner_panel, course_examiner_panel_member, flashMessage: Flash.getMessage(req) })
            }
        } else if (course_examiner_panel.length === 0) {
            if (course_instructor_panel.length === 0) {
                return res.render('pages/admin/viewExamCommitteeEmptyInstructorAndExaminerPanel', { title: 'Exam Committe Details', user: req.user, data, course_instructor_panel, course_examiner_panel, flashMessage: Flash.getMessage(req) })
            } else {
                const course_instructor_panel_id = course_instructor_panel[0].id;
                const course_instrutor_panel_member = await Course_Instructor_Panel.findByPk(course_instructor_panel_id, {
                    include: [{
                        model: Course_Instructor_Panel_Member,
                        where: { status: '1' },
                        include: [{ model: Users, as: 'internal_instructor' }, { model: Users, as: 'external_instructor' }, { model: Course }]
                    }]
                })
                return res.render('pages/admin/viewExamCommitteeInstructorPanelWithEmptyExaminer', { title: 'Exam Committee Details', user: req.user, data, course_instructor_panel, course_instrutor_panel_member, course_examiner_panel, flashMessage: Flash.getMessage(req) })
            }
        } else {
            const course_instructor_panel_id = course_instructor_panel[0].id;
            const course_instrutor_panel_member = await Course_Instructor_Panel.findByPk(course_instructor_panel_id, {
                include: [{
                    model: Course_Instructor_Panel_Member,
                    where: { status: '1' },
                    include: [{ model: Users, as: 'internal_instructor' }, { model: Users, as: 'external_instructor' }, { model: Course }]
                }]
            })

            const course_examiner_panel_id = course_examiner_panel[0].id;
            const course_examiner_panel_member = await Course_Examiner_Panel.findByPk(course_examiner_panel_id, {
                include: [{
                    model: Course_Examiner_Panel_Member,
                    where: { status: '1' },
                    include: [{ model: Users, as: 'internal_examiner' }, { model: Users, as: 'external_examiner' }, { model: Users, as: 'third_examiner' }, { model: Course }]
                }]
            })
            console.log("this is data", course_instrutor_panel_member.course_instructor_panel_members[0])
                //console.log("course_examiner_panel_member",course_examiner_panel_member.course_examiner_panel_members[0].id)
            return res.render('pages/admin/viewExamCommitteeDetails', { title: 'Exam Committee Details', user: req.user, data, course_instructor_panel, course_instrutor_panel_member, course_examiner_panel, course_examiner_panel_member, flashMessage: Flash.getMessage(req) })

        }

    } catch (e) {
        console.log(e)
    }
}

// disable exam commmittee api creation 
exports.disableExamCommittee = async(req, res, next) => {
    try {
        let id = req.params.id
        let response = await Exam_Committees.update({
            status: '0'
        }, {
            where: {
                id: id
            }
        })
        return res.redirect('/admin/view-exam-committee')
    } catch (e) {
        console.log(e)
    }
}

// get edit exam committee page 
exports.getEditExamCommitteePage = async(req, res, next) => {
    // get all committee type 
    try {
        let exam_committee_id = req.params.id
        const committee_roles = await Exam_Committee_Roles.findAll()
        const sessions = await Session.findAll()
        const users = await Users.findAll({
            where: {
                role: '2'
            }
        })


        const data = await Exam_Committees.findByPk(exam_committee_id, {
            include: [{
                model: Exam_Committee_Members,
                where: { status: '1' },
                include: [{ model: Users }, { model: Exam_Committee_Roles }]
            }]
        })

        //return res.json(data)
        return res.render('pages/admin/editExamCommitteePage', { title: 'Edit Exam Committee', data, committee_roles, users, user: req.user, sessions, flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }
}

// post edit exam committee

exports.postEditExamCommitteePage = async(req, res, next) => {
    // get all committee type 
    try {
        let exam_committee_id = req.params.id
        const data = await Exam_Committees.findByPk(exam_committee_id, {
            include: [{
                model: Exam_Committee_Members,
                where: { status: '1' },
                include: [{ model: Users }, { model: Exam_Committee_Roles }]
            }]
        })

        if (data.session != req.body.session || data.year !== req.body.year || data.semester != req.body.semester) {
            await Exam_Committees.update({
                session: req.body.session,
                year: req.body.year,
                semester: req.body.semester
            }, {
                where: {
                    id: exam_committee_id
                }
            })
        }

        let committee_members = []
        let current_members;
        for (let i = 0; i < req.body.exam_committee_roles.length; i++) {
            current_members = {}
            current_members['id'] = req.body.id[i]
            current_members['exam_committee_id'] = exam_committee_id
            current_members['member_id'] = req.body.exam_committee_members[i]
            current_members['member_role'] = req.body.exam_committee_roles[i]
            current_members['status'] = '1'
            committee_members.push(current_members)
        }

        await Exam_Committee_Members.bulkCreate(committee_members, {
                updateOnDuplicate: ['member_id', 'member_role'],
                returning: true
            })
            .then(() => {
                req.flash('success', 'Exam Committee Updated Successfully')
                return res.redirect('/admin/view-exam-committee')
            })
            .catch(() => {
                req.flash('fail', 'Exam Committee Update Failed')
                return res.redirect('/admin/view-exam-committee')
            })
    } catch (e) {
        console.log(e)
    }
}

// disable exam committee member 
exports.disableExamCommitteeMember = async(req, res, next) => {
    try {
        let exam_committee_members = req.params.id
        let response = await Exam_Committee_Members.update({
                status: '0'
            }, {
                where: {
                    id: exam_committee_members
                }
            })
            //return res.redirect('/admin/view-exam-committee')
        res.json(response)
    } catch (e) {
        console.log(e)
        next()
    }
}

//get add course instruction page 

exports.createCourseInstructorPanel = async(req, res, next) => {
    try {
        const exam_committee_id = req.params.id;
        const exam_committee = await Exam_Committees.findByPk(exam_committee_id);
        const department = exam_committee.department_name;
        const year = exam_committee.year;
        const semester = exam_committee.semester;
        const session = exam_committee.session;
        const sessions = await Session.findAll()
        const course = await Course.findAll({
            where: {
                department_name: department,
                session_name: session,
                [Op.or]: [{
                        course_code: {
                            [Op.like]: `% ${year}${semester}%`
                        }
                    },
                    {
                        course_code: {
                            [Op.like]: `%-${year}${semester}%`
                        }
                    }
                ]
            }
        })
        const external = await Users.findAll({
            where: {
                [Op.and]: [
                    { role: '2' },
                    // {
                    //     email: {
                    //         [Op.notLike]: '%@just%'
                    //     }
                    // }

                ]
            }
        })

        const internal = await Users.findAll({
            where: {
                [Op.and]: [
                    { role: '2' },
                    // {
                    //     email: {
                    //         [Op.like]: '%@just%'
                    //     }
                    // }
                ]
            }
        })
        return res.render('pages/admin/createCourseInstructorPanel', { sessions, course, external, internal, exam_committee, title: "Create Course Instructor Panel", user: req.user, flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }
}

exports.createCourseInstructorPanelPostController = async(req, res, next) => {
    try {
        const exam_committee_id = req.params.id
        const exam_committee = await Exam_Committees.findByPk(exam_committee_id);
        let department = exam_committee.department_name;
        let session = exam_committee.session;
        let year = exam_committee.year;
        let semester = exam_committee.semester;

        const course_instructor_panel = await Course_Instructor_Panel.create({
            exam_committee_id: exam_committee_id,
            department_name: department,
            session: session,
            year: year,
            semester: semester,
            status: '1'
        })
        if (course_instructor_panel) {
            let course_instructor_panel_member = []
            let instructor;
            for (let i = 0; i < req.body.course.length; i++) {
                instructor = {}
                instructor['course_instructor_panel_id'] = course_instructor_panel.id
                instructor['course_id'] = req.body.course[i]
                instructor['internal'] = req.body.internal[i]
                instructor['external'] = req.body.external[i]
                instructor['status'] = '1'
                course_instructor_panel_member.push(instructor)
            }

            const generated_course_instructor_panel_member = await Course_Instructor_Panel_Member.bulkCreate(course_instructor_panel_member, { returning: true })
                .then(() => {
                    req.flash('success', 'Instructor Panel Created')
                    return res.redirect('/')
                }).catch(() => {
                    req.flash('fail', 'Instructor Panel Create Failed')
                    return res.redirect('/')
                })
        } else {
            req.flash('fail', 'Instructor Panel Create Failed')
            return res.redirect('/')
        }
    } catch (e) {
        console.log(e)
    }
}

exports.viewCourseInstructorPanelPage = async(req, res, next) => {
    try {
        const course_instructor_panel = await Course_Instructor_Panel.findAll({
            where: {
                department_name: req.user.department_name,
                status: '1'
            }
        })
        return res.render('pages/admin/viewCourseInstructorPanel', { course_instructor_panel, title: "View Course Instructor Panel", user: req.user, flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }

}

exports.viewCourseInstructorPanelPageDetails = async(req, res, next) => {
    try {
        const course_instructor_panel_id = req.params.id
        const data = await Course_Instructor_Panel.findByPk(course_instructor_panel_id, {
            include: [{
                model: Course_Instructor_Panel_Member,
                where: { status: '1' },
                include: [{ model: Users, as: 'internal_instructor' }, { model: Users, as: 'external_instructor' }, { model: Course }]
            }]
        })

        return res.render('pages/admin/viewCourseInstructorPanelDetails', { data, user: req.user, title: 'Course Instructor Panel Details', flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }
}



//disable course instructor panel
exports.disableCourseInstructorPanel = async(req, res, next) => {
    try {
        let id = req.params.id
        let panel = await Course_Instructor_Panel.update({
            status: '0'
        }, {
            where: {
                id: id
            }
        })
        return res.redirect('/')
        console.log(id)
    } catch (e) {
        console.log(e)
    }
}


// get edit couser instructor panel page 
exports.getEditCourseInstructorPanelPage = async(req, res, next) => {
    // get all instructor panel 
    try {
        let panel_id = req.params.id
        const exam_committe = await Course_Instructor_Panel.findByPk(panel_id);
        const exam_committe_panel_id = exam_committe.exam_committee_id;
        const exam_committe_panel = await Exam_Committees.findByPk(exam_committe_panel_id);
        const department = exam_committe_panel.department_name;
        const session = exam_committe_panel.session;
        const year = exam_committe_panel.year;
        const semester = exam_committe_panel.semester;
        console.log("this is the---", exam_committe_panel);
        const sessions = await Session.findAll()
        const course = await Course.findAll({
            where: {
                department_name: department,
                session_name: session,
                [Op.or]: [{
                        course_code: {
                            [Op.like]: `% ${year}${semester}%`
                        }
                    },
                    {
                        course_code: {
                            [Op.like]: `%-${year}${semester}%`
                        }
                    }
                ]
            }
        })
        const internal = await Users.findAll({
            where: {

                role: '2',


            }
        })
        const external = await Users.findAll({
            where: {

                role: '3',


            }
        })

        const data = await Course_Instructor_Panel.findByPk(panel_id, {
            include: [{
                model: Course_Instructor_Panel_Member,
                where: { status: '1' },
                include: [{ model: Users, as: 'internal_instructor' }, { model: Users, as: 'external_instructor' }, { model: Course }]
            }]
        })

        //return res.json(data)
        return res.render('pages/admin/editCourseInstructorPanelPage', { title: 'Edit Course Instructor Panel', data, exam_committe_panel, course, internal, external, user: req.user, sessions, flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }
}


exports.postEditCourseInstructorPanelPage = async(req, res, next) => {
    // get all instructor panel  
    try {
        let panel_id = req.params.id
        const data = await Course_Instructor_Panel.findByPk(panel_id, {
            include: [{
                model: Course_Instructor_Panel_Member,
                where: { status: '1' },
                include: [{ model: Users, as: 'internal_instructor' }, { model: Users, as: 'external_instructor' }, { model: Course }]
            }]
        })

        let exam_committee_id = data.exam_committee_id

        if (data.session != req.body.session || data.year !== req.body.year || data.semester != req.body.semester) {
            await Course_Instructor_Panel.update({
                session: req.body.session,
                year: req.body.year,
                semester: req.body.semester
            }, {
                where: {
                    id: panel_id
                }
            })
        }
        let course_instructor_panel_member = []
        let instructor;
        for (let i = 0; i < req.body.course.length; i++) {
            instructor = {}
            instructor['id'] = req.body.id[i]
            instructor['exam_committee_id'] = exam_committee_id
            instructor['course_instructor_panel_id'] = panel_id
            instructor['course_id'] = req.body.course[i]
            instructor['internal'] = req.body.internal[i]
            instructor['external'] = req.body.external[i]
            instructor['status'] = '1'
            course_instructor_panel_member.push(instructor)
        }
        await Course_Instructor_Panel_Member.bulkCreate(course_instructor_panel_member, {
            updateOnDuplicate: ['course_id', 'internal', 'external'],
            returning: true
        }).then(() => {
            req.flash('success', 'Instructor Panel Updated')
            return res.redirect('/')
        }).catch(() => {
            req.flash('fail', 'Instructor Panel Update Failed')
            return res.redirect('/')
        })

    } catch (e) {
        console.log(e)
    }
}


// disable course Instructor Panel Member 
exports.disableCourseInstructorPanelMember = async(req, res, next) => {
    try {
        let panel_id = req.params.id
        let response = await Course_Instructor_Panel_Member.update({
                status: '0'
            }, {
                where: {
                    id: panel_id
                }
            })
            //return res.redirect('/admin/view-exam-committee')
        res.json(response)
    } catch (e) {
        console.log(e)
    }
}

/** Examiner Panel Create Page 
 * @version 27-9-22
 * @since 27-9-22
 * 
 * @param 
 *  $GET= [$exam_committee_id]
 * 
 */

exports.createCourseExaminerPanel = async(req, res, next) => {
    try {
        const exam_committee_id = req.params.id;
        const exam_committee = await Exam_Committees.findByPk(exam_committee_id);
        const year = exam_committee.year;
        const semester = exam_committee.semester;
        const session = exam_committee.session;
        const department = exam_committee.department_name;
        const question_info = await Question_Informations.findAll({
            where: {
                department: department,
                session: session,
                year: year,
                semester: semester
            }
        })
        const external = await Users.findAll({
            where: {
                [Op.and]: [
                    { role: '2' },
                    // {
                    //     email: {
                    //         [Op.notLike]: '%@just%'
                    //     }
                    // }

                ]
            }
        })

        const internal = await Users.findAll({
            where: {
                [Op.and]: [
                    { role: '2' },
                    // {
                    //     email: {
                    //         [Op.like]: '%@just%'
                    //     }
                    // }
                ]
            }
        })
        return res.render('pages/admin/createExaminerPanel', { question_info, external, internal, exam_committee, title: "Create Examiner Panel", user: req.user, flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }
}

/** Creaet Examiner Panel  
 * @version 3-10-22
 * @since 3-10-22
 * 
 * @param 
 *  $POST= [$exam_committee_id]
 * 
 */

exports.createExaminerPanelPostController = async(req, res, next) => {
    try {
        const exam_committee_id = req.params.id
        const exam_committee = await Exam_Committees.findByPk(exam_committee_id);
        let department = exam_committee.department_name;
        let session = exam_committee.session;
        let year = exam_committee.year;
        let semester = exam_committee.semester;

        const course_examiner_panel = await Course_Examiner_Panel.create({
            exam_committee_id: exam_committee_id,
            department_name: department,
            session: session,
            year: year,
            semester: semester,
            status: '1'
        })

        if (course_examiner_panel) {
            let course_examiner_panel_member = []
            let examiner;

            for (let i = 0; i < req.body.course.length; i++) {
                examiner = {}
                examiner['course_examiner_panel_id'] = course_examiner_panel.id
                examiner['course_id'] = req.body.course[i]
                examiner['internal'] = req.body.internal[i]
                examiner['external'] = req.body.external[i]
                examiner['third'] = req.body.third[i]
                examiner['status'] = '1'
                course_examiner_panel_member.push(examiner)
            }
            console.log(course_examiner_panel_member)
            const generated_course_examiner_panel_member = await Course_Examiner_Panel_Member.bulkCreate(course_examiner_panel_member, { returning: true })
                .then(() => {
                    req.flash('success', 'Examiner Panel Created')
                    return res.redirect('/')
                }).catch((e) => {
                    req.flash('fail', 'Examiner Panel Create Failed')
                    console.log(e)
                    return res.redirect('/')
                })

        } else {
            req.flash('fail', 'Examiner Panel Create Failed')
            return res.redirect('/')
        }
    } catch (e) {
        console.log(e)
    }
}


/** Edit Examiner Panel  
 * @version 4-10-22
 * @since 4-10-22
 * 
 * @param 
 *  $GET= [$examiner_panel_id]
 * 
 */

exports.getEditCourseExaminerPanelPage = async(req, res, next) => {
    // get all instructor panel 
    try {
        let examiner_panel_id = req.params.id
        const exam_committe = await Course_Examiner_Panel.findByPk(examiner_panel_id);
        const exam_committe_panel_id = exam_committe.exam_committee_id;
        const exam_committe_panel = await Exam_Committees.findByPk(exam_committe_panel_id);
        const department = exam_committe_panel.department_name;
        const session = exam_committe_panel.session;
        const year = exam_committe_panel.year;
        const semester = exam_committe_panel.semester;
        const sessions = await Session.findAll()
        const question_info = await Question_Informations.findAll({
            where: {
                department: department,
                session: session,
                year: year,
                semester: semester
            }
        })
        const internal = await Users.findAll({
            where: {
                [Op.and]: [
                    { role: '2' },
                    {
                        email: {
                            [Op.like]: '%@just%'
                        }
                    }
                ]
            }
        })
        const external = await Users.findAll({
            where: {
                [Op.and]: [
                    { role: '2' },
                    {
                        email: {
                            [Op.notLike]: '%@just%'
                        }
                    }
                ]
            }
        })

        const data = await Course_Examiner_Panel.findByPk(examiner_panel_id, {
            include: [{
                model: Course_Examiner_Panel_Member,
                where: { status: '1' },
                include: [{ model: Users, as: 'internal_examiner' }, { model: Users, as: 'external_examiner' }, { model: Users, as: 'third_examiner' }, { model: Course }]
            }]
        })

        //return res.json(data)
        return res.render('pages/admin/editCourseExaminerPanelPage', { title: 'Edit Examiner Panel', data, question_info, internal, exam_committe_panel, external, user: req.user, sessions, flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }
}


/** Edit Examiner Panel  
 * @version 4-10-22
 * @since 4-10-22
 * 
 * @param 
 *  $POST= [$examiner_panel_id]
 * 
 */

exports.postEditCourseExaminerPanelPage = async(req, res, next) => {
    // get all instructor panel  
    try {
        let examiner_panel_id = req.params.id
        const data = await Course_Examiner_Panel.findByPk(examiner_panel_id, {
            include: [{
                model: Course_Examiner_Panel_Member,
                where: { status: '1' },
                include: [{ model: Users, as: 'internal_examiner' }, { model: Users, as: 'external_examiner' }, { model: Users, as: 'third_examiner' }, { model: Course }]
            }]
        })

        let exam_committee_id = data.exam_committee_id

        if (data.session != req.body.session || data.year !== req.body.year || data.semester != req.body.semester) {
            await Course_Examiner_Panel.update({
                session: req.body.session,
                year: req.body.year,
                semester: req.body.semester
            }, {
                where: {
                    id: examiner_panel_id
                }
            })
        }
        let course_examiner_panel_member = []
        let examiner;
        for (let i = 0; i < req.body.course.length; i++) {
            examiner = {}
            examiner['id'] = req.body.id[i]
            examiner['exam_committee_id'] = exam_committee_id
            examiner['course_examiner_panel_id'] = examiner_panel_id
            examiner['course_id'] = req.body.course[i]
            examiner['internal'] = req.body.internal[i]
            examiner['external'] = req.body.external[i]
            examiner['third'] = req.body.third[i]
            examiner['status'] = '1'
            course_examiner_panel_member.push(examiner)
        }
        await Course_Examiner_Panel_Member.bulkCreate(course_examiner_panel_member, {
                updateOnDuplicate: ['course_id', 'internal', 'external', 'third'],
                returning: true
            })
            .then(() => {
                req.flash('success', 'Examiner Panel Updated')
                return res.redirect('/admin/view-exam-committee')
            }).catch(() => {
                req.flash('fail', 'Examiner Panel Update Failed')
                return res.redirect(`/admin/view-exam-committee`)
            })
    } catch (e) {
        console.log(e)
    }
}


/** 
 * Diable Examiner Panel  
 * @version 4-10-22
 * @since 4-10-22
 * 
 */

exports.disableCourseExaminerPanel = async(req, res, next) => {
    try {
        let examiner_panel_id = req.params.id
        let response = await Course_Examiner_Panel.update({
                status: '0'
            }, {
                where: {
                    id: examiner_panel_id
                }
            })
            //return res.redirect('/admin/view-exam-committee')
        res.json(response)
    } catch (e) {
        console.log(e)
    }
}


/** 
 *Register Student
 * @version 27-11-22
 * @since 27-11-22
 * @param 
 *  $GET
 */

exports.getStudentRegisterPage = async(req, res, next) => {
    try {
        let id = req.params.id;
        let course_instructor_panel_member_id = req.params.course_instructor_panel_member_id;
        let course_id = req.params.course_id;
        return res.render('pages/admin/studentRegisterPage', { student: {}, title: "Register Student", id, course_instructor_panel_member_id, course_id, user: req.user, flashMessage: Flash.getMessage(req) })

    } catch (e) {
        console.log(e)
    }
}

/** Student File Upload 
 * @version 27-11-22
 * @since 27-11-22
 * @param 
 *  $GET
 * 
 */

exports.getStudentRegisterPageController = async(req, res, next) => {
    try {
        let id = req.params.id;
        let course_instructor_panel_member_id = req.params.course_instructor_panel_member_id;
        let course_id = req.params.course_id;
        console.log("id no is", id);
        return res.render('pages/admin/studentRegisterFile', { title: "Student Id FileUpload", id, course_instructor_panel_member_id, course_id, user: req.user, flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }
}


/** Student File Submit 
 * @version 28-11-22
 * @since 28-11-22
 * @param 
 *  $POST
 * 
 */
exports.postStudentRegisterPageController = async(req, res, next) => {
    try {
        let id = req.params.id;
        let course_instructor_panel_member_id = req.params.course_instructor_panel_member_id;
        let course_id = req.params.course_id;
        if (req.file == undefined) {
            req.flash('fail', 'Enter student Id file');
        } else {
            let path = 'public/uploads/' + req.file.filename
            let department_name = req.body.department
            readXlsxFile(path).then((rows) => {
                rows.shift();
                let students = []
                rows.forEach((row) => {
                    let student = {
                        department_name: department_name,
                        student_id: row[0],

                    }
                    students.push(student)

                })

                res.render('pages/admin/createStudentRegister', { student: students, title: "Add Students", id, course_instructor_panel_member_id, course_id, user: req.user, flashMessage: Flash.getMessage(req) })
            })
        }

    } catch (e) {
        console.log(e)
        res.send({
            message: "file upload fail" + req.file.originalname
        })
    }

}


/** Student File Submit 
 * @version 28-11-22
 * @since 28-11-22
 * @param 
 *  $POST[$exam_committee_id,$course_instructor_panel_member_id,$course_id]
 * 
 */

exports.postSubmitStudentRegisterController = async(req, res, next) => {
    try {
        let id = req.params.id
        let course_instructor_panel_member_id = req.params.course_instructor_panel_member_id;
        let course_id = req.params.course_id;
        let students = [];
        let student = {};

        for (let i = 0; i < req.body.student_id.length; i++) {
            student['department_name'] = req.body.department;
            student['exam_committee_id'] = id;
            student['course_instructor_panel_member_id'] = course_instructor_panel_member_id;
            student['course_id'] = course_id;
            student['student_id'] = req.body.student_id[i];

            students.push(student)
            student = {}
        }

        await Student.bulkCreate(students, {
                ignoreDuplicates: true
            }).then(() => {
                req.flash('success', 'Student Added Successfully')
                res.redirect('/')
            })
            .catch(() => {
                req.flash('fail', 'Student cannot be added')
            })
    } catch (e) {
        console.log(e);
    }
}

/** Get Courses which questions are already made
 * @version 02-12-22
 * @since 02-12-22
 * @param 
 *  $Get[$session,$year,$semester]
 * 
 */

exports.getExamineCourses = async(req, res, next) => {
    try {
        const department = req.params.department;
        const session = req.params.session;
        const year = req.params.year;
        const semester = req.params.semester;
        const question_info = await Question_Informations.findAll({
            where: {
                department: department,
                session: session,
                year: year,
                semester: semester
            }
        });
        return res.json(question_info)
    } catch (e) {
        console.log(e);
    }
}