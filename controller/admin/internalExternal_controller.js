const db = require('../../model/database')
const Users = db.users
const { Op } = require("sequelize");
const Flash=require('../../utils/Flash')

exports.getAddInternalExternalPage = async(req, res, next) => {
    try {

        return res.render('pages/admin/internal_external/addInternalExternal', { title: "Add Internal-External", user: req.user,flashMessage:Flash.getMessage(req) })

    } catch (e) {
        console.log(e)
        next(e)
    }
}


exports.postAddInternalExternalPage = async(req, res, next) => {
    try {
        let { name, email, phone_number, bank_account, department_name, current_designation, university_name, role } = req.body
        let createdUser = await Users.create({
            googleId: "",
            name,
            avatar: "",
            department_id: "",
            student_id: "",
            session: "",
            email,
            phone_number,
            bank_account,
            current_designation,
            department_name,
            university_name,
            role,
            status: "1"
        })
        if (createdUser) {
            req.flash('success','Add User Successfull')
            return res.redirect("/")
        } else {
            alert("Something wrong")
        }
    } catch (e) {
        next(e)
        console.log(e)
    }
}


exports.getAllInternalExternal = async(req, res, next) => {
    try {
        const data = await Users.findAll({
            where: {
                [Op.or]: [{ role: '2' }, { role: '3' }],
                // [Op.and]: [{ status: '1' }]
            }
        });
        //return res.json(data)
        return res.render('pages/admin/internal_external/viewInternalExternal', { title: "Internal-External", data, user: req.user,flashMessage:Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
        next(e)
    }
}


exports.getSingleInternalExternal = async(req, res, next) => {
    try {
        const userId = req.params.id
        const data = await Users.findByPk(userId)
            //return res.json(user)
        return res.render('pages/admin/internal_external/viewInternalExternalProfile', { title: "Internal-External Profile", data, user: req.user,flashMessage:Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
        next(e)
    }
}

exports.enableDisableInternalExternal = async(req, res, next) => {
    try {
        let internalExternalId = req.params.id
        let active = null
        let internalExternal = await Users.findByPk(internalExternalId)
        if (!internalExternal) {
            return res.json({ msg: 'Internal/External not found' })
        }

        if (internalExternal.status == "1") {
            await Users.update({
                status: '0'
            }, {
                where: {
                    id: internalExternalId
                }
            })
            active = false
            return res.status(200).json({ active })
        } else {
            await Users.update({
                status: '1'
            }, {
                where: {
                    id: internalExternalId
                }
            })
            active = true
            return res.status(200).json({ active })
        }

    } catch (e) {
        console.log(e)
        next(e)
    }
}

exports.updateIinternalExternal = async(req, res, next) => {
    try {

        let internalExternalId = req.params.id
        let { name, phone_number, bank_account, department_name, current_designation, university_name, role } = req.body
        let updatedProfile = await Users.update({
            name,
            phone_number,
            bank_account,
            department_name,
            current_designation,
            university_name,
            role
        }, {
            where: {
                id: internalExternalId
            }
        })
        if (updatedProfile) {
            return res.redirect(`/admin/view-internal-external/profile/${internalExternalId}`)
        }

    } catch (e) {
        console.log(e)
        next(e)
    }
}