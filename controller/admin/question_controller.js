const db = require('../../model/database')
const Question_Informations = db.question_informations
const Questions = db.questions
const Exam_Committee_Roles = db.exam_committee_roles
const Users = db.users
const Course = db.courses
const Exam_Committee_Members = db.exam_committee_members
const Exam_Committees = db.exam_committee
const Course_Instructor_Panel = db.course_instructor_panel
const Course_Instructor_Panel_Member = db.course_instructor_panel_member
const { Op } = require("sequelize");
const Flash = require('../../utils/Flash')


exports.getModerateQuestionController = async(req, res, next) => {
    try {
        const course_instructor_panel_id = req.params.id
        const question_type = req.params.type
        const uuid = req.params.uuid

        const questions = await Question_Informations.findAll({
            where: {
                course_instructor_panel_id: course_instructor_panel_id,
                question_type: question_type,
                uuid: uuid
            },
            include: [{
                model: Questions
            }]
        })

        //res.json(questions)
        return res.render('pages/admin/question/viewQuestions', { questions, user: req.user, title: 'Moderated Question', flashMessage: Flash.getMessage(req) })

    } catch (e) {
        console.log(e)
        next(e)
    }
}

exports.getQuestionsController = async(req, res, next) => {

    try {

        const course_instructor_panel_id = req.params.id
        let question_type = req.params.type
        const uuid = req.params.uuid
            //console.log(question_type === "moderate")

        if (question_type === "moderate") {
            const _questions = await Question_Informations.findAll({
                where: {
                    course_instructor_panel_id: course_instructor_panel_id,
                    uuid: uuid,
                    [Op.or]: [{ question_type: 'internal' }, { question_type: 'external' }]
                },
                // order: [
                //     ['question_type', 'DESC'],
                // ],
                include: [{
                    model: Questions
                }]
            })

            let questions = []

            if (_questions[0].question_type == 'internal') {
                questions[0] = _questions[0]
                questions[1] = _questions[1]
            } else if (_questions[0].question_type == 'external') {
                questions[0] = _questions[1]
                questions[1] = _questions[0]
            }


            const moderateQuestions = await Question_Informations.findAll({
                where: {
                    course_instructor_panel_id: course_instructor_panel_id,
                    uuid: uuid,
                    question_type: question_type
                },
                include: [{
                    model: Questions
                }]
            })

            //return res.json(question)
            return res.render('pages/admin/question/modarateQuestion', { questions, moderateQuestions, user: req.user, title: 'Question Details', question_type, flashMessage: Flash.getMessage(req) })
        } else {
            const questions = await Question_Informations.findAll({
                where: {
                    course_instructor_panel_id: course_instructor_panel_id,
                    question_type: question_type,
                    uuid: uuid
                },
                include: [{
                    model: Questions
                }]
            })

            //return string.charAt(0).toUpperCase() + string.slice(1);

            question_type = question_type.charAt(0).toUpperCase() + question_type.slice(1)

            //res.json(questions)
            return res.render('pages/admin/question/viewQuestions', { questions, user: req.user, title: `${question_type} Question`, flashMessage: Flash.getMessage(req) })
        }


    } catch (e) {
        console.log(e)
        next(e)
    }
}


exports.postModerateQuestionsController = async(req, res, next) => {
    try {
        let type = req.params.type
        let uuid = req.params.uuid
        let course_instructor_panel_id = req.params.id

        // ......................................................................

        let questions_info = await Question_Informations.findAll({
                where: {
                    question_type: type,
                    uuid: uuid
                },
                include: [{
                    model: Questions
                }]
            })
            //console.log(questions)

        if (questions_info.length != 0) {
            //return res.json(questions_info[0].questions[0].question_information_id)
            let question_array = []
            let current_question;
            for (let i = 0; i < req.body.qstn_number.length; i++) {
                current_question = {}
                current_question['id'] = questions_info[0].questions[i].id
                current_question['question_information_id'] = questions_info[0].questions[0].question_information_id
                current_question['question_number'] = req.body.qstn_number[i]
                current_question['question_section'] = req.body.qstn_section[i]
                current_question['question'] = req.body.qstn[i]
                current_question['question_marks'] = req.body.qstn_marks[i]
                question_array.push(current_question)
            }

            await Questions.bulkCreate(question_array, { updateOnDuplicate: ['question', 'question_marks'], returning: true })

            return res.json({
                msg: 'Question Update Success'
            })

        }

        // ......................................................................


        const info = await Course_Instructor_Panel.findByPk(course_instructor_panel_id, {
            include: [{
                model: Course_Instructor_Panel_Member,
                where: { status: '1', uuid: uuid },
                include: [{ model: Users, as: 'internal_instructor' }, { model: Users, as: 'external_instructor' }, { model: Course }]
            }]
        })


        let course_instructor_panel = course_instructor_panel_id
        let question_type = type
        let year = info.year
        let semester = info.semester
        let session = info.session
        let department = info.department_name
        let course_title = info.course_instructor_panel_members[0].course.course_title
        let course_code = info.course_instructor_panel_members[0].course.course_code
        let course_credit = info.course_instructor_panel_members[0].course.theory_credit
        let full_marks = info.course_instructor_panel_members[0].course.see


        //console.log(question_array)

        const create_question_information = await Question_Informations.create({
            course_instructor_panel_id: course_instructor_panel,
            question_type: question_type,
            uuid,
            year: year,
            semester: semester,
            session: session,
            department: department,
            course_title: course_title,
            course_code: course_code,
            course_credit: course_credit,
            full_marks: full_marks
        })
        if (create_question_information) {
            let question_array = []
            let current_question;
            for (let i = 0; i < req.body.qstn_number.length; i++) {
                current_question = {}
                current_question['question_information_id'] = create_question_information.id
                current_question['question_number'] = req.body.qstn_number[i]
                current_question['question_section'] = req.body.qstn_section[i]
                current_question['question'] = req.body.qstn[i]
                current_question['question_marks'] = req.body.qstn_marks[i]
                question_array.push(current_question)
            }
            //const create_question = await Questions.bulkCreate(question_array, {updateOnDuplicate: ['questions', 'question_marks'], returning: true })
            const create_question = await Questions.bulkCreate(question_array, { returning: true })
            if (!create_question) {
                return res.json({ msg: 'question create fail' })
            }
        } else {
            return res.json({ msg: 'question create fail' })
        }
        console.log('question create success')
            //return res.redirect('/teacher/add-question')
        return res.json('Qstn create success')


    } catch (e) {
        console.log(e)
        next(e)
    }
}


exports.get_all_questions = async(req, res, next) => {
    try {
        const course_instructor_panel_id = req.params.id
        let question_type = req.params.type
        const uuid = req.params.uuid
            //console.log(question_type === "moderate")

        if (question_type === "moderate") {
            const _questions = await Question_Informations.findAll({
                where: {
                    course_instructor_panel_id: course_instructor_panel_id,
                    uuid: uuid,
                    [Op.or]: [{ question_type: 'internal' }, { question_type: 'external' }]
                },
                // order: [
                //     ['question_type', 'DESC'],
                // ],
                include: [{
                    model: Questions
                }]
            })

            let questions = []

            if (_questions[0].question_type == 'internal') {
                questions[0] = _questions[0]
                questions[1] = _questions[1]
            } else if (_questions[0].question_type == 'external') {
                questions[0] = _questions[1]
                questions[1] = _questions[0]
            }


            const moderateQuestions = await Question_Informations.findAll({
                where: {
                    course_instructor_panel_id: course_instructor_panel_id,
                    uuid: uuid,
                    question_type: question_type
                },
                include: [{
                    model: Questions
                }]
            })

            return res.json({ questions, moderateQuestions })
                //return res.render('pages/admin/question/modarateQuestion', { questions, moderateQuestions, user: req.user, title: 'Question Details', question_type })
        } else {
            const questions = await Question_Informations.findAll({
                where: {
                    course_instructor_panel_id: course_instructor_panel_id,
                    question_type: question_type,
                    uuid: uuid
                },
                include: [{
                    model: Questions
                }]
            })

            //return string.charAt(0).toUpperCase() + string.slice(1);

            question_type = question_type.charAt(0).toUpperCase() + question_type.slice(1)

            res.json(questions)
                //return res.render('pages/admin/question/viewQuestions', { questions, user: req.user, title: `${question_type} Question` })
        }

    } catch (e) {
        console.log(e)
        next(e)
    }
}