const db = require('../../model/database')
const Course = db.courses
const Exam_Committee_Roles = db.exam_committee_roles
const Users = db.users
const course = require('../../model/course')
const Session = db.sessions
const Exam_Committee_Members = db.exam_committee_members
const Exam_Committees = db.exam_committee
const Course_Instructor_Panel = db.course_instructor_panel
const Course_Instructor_Panel_Member = db.course_instructor_panel_member
const Course_Examiner_Panel = db.course_examiner_panel
const Course_Examiner_Panel_Member = db.course_examiner_panel_member
const Question_Information=db.question_informations
const { Op } = require("sequelize");
const { v4: uuid4 } = require('uuid')
const Flash=require('../../utils/Flash')

exports.getAllInstractorList = async(req, res, next) => {
    try {
        const course_instructor_panel_id = req.params.id
        const data = await Course_Instructor_Panel.findByPk(course_instructor_panel_id, {
            include: [{
                model: Course_Instructor_Panel_Member,
                where: { status: '1' },
                include: [{ model: Users, as: 'internal_instructor' }, { model: Users, as: 'external_instructor' }, { model: Course }]
            }]
        })

        //console.log(data.course_examiner_panel_members[0])
        return res.render('pages/admin/allInstractorList.ejs', { data, user: req.user, title: 'Course Instractor List',flashMessage:Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }
}

exports.emailSendController = async(req, res, next) => {
    try {


        const course_instructor_panel_id = req.params.id
        let date = new Date(req.body.lastDate);

        const data = await Course_Instructor_Panel_Member.findAll({
            where: {
                course_instructor_panel_id: course_instructor_panel_id
            }
        })

        let course_instructor_panel_member = []
        let instructor;
        for (let i = 0; i < data.length; i++) {
            instructor = {}
            let uuid = uuid4();
            instructor['id'] = data[i].id,
                instructor['course_instructor_panel_id'] = course_instructor_panel_id,
                instructor['course_id'] = data[i].course_id,
                instructor['internal'] = data[i].internal,
                instructor['external'] = data[i].external,
                instructor['uuid'] = uuid,
                instructor['uuid_expire_date'] = date,
                instructor['status'] = '1',
                course_instructor_panel_member.push(instructor)
        }
        console.log(course_instructor_panel_member)
        await Course_Instructor_Panel_Member.bulkCreate(course_instructor_panel_member, {
            updateOnDuplicate: ['course_id', 'internal', 'external', 'uuid', 'uuid_expire_date'],
            returning: true
        })



        const info = await Course_Instructor_Panel.findByPk(course_instructor_panel_id, {
            include: [{
                model: Course_Instructor_Panel_Member,
                where: { status: '1' },
                include: [{ model: Users, as: 'internal_instructor' }, { model: Users, as: 'external_instructor' }, { model: Course }]
            }]
        })







        let instructor_list = [];
        let _instructor;
        //console.log(info.course_examiner_panel_members.length)
        //return res.json(info)

        for (let i = 0; i < info.course_instructor_panel_members.length; i++) {
            _instructor = {}
            _instructor['role'] = 'internal'
            _instructor['uuid'] = info.course_instructor_panel_members[i].uuid
            _instructor['uuid_expire_date'] = info.course_instructor_panel_members[i].uuid_expire_date
            _instructor['name'] = info.course_instructor_panel_members[i].internal_instructor.name;
            _instructor['email'] = info.course_instructor_panel_members[i].internal_instructor.email;
            _instructor['department_name'] = info.course_instructor_panel_members[i].course.department_name;
            _instructor['course_code'] = info.course_instructor_panel_members[i].course.course_code;
            _instructor['session'] = info.course_instructor_panel_members[i].course.session_name;
            _instructor['course_title'] = info.course_instructor_panel_members[i].course.course_title;
            _instructor['course_type'] = info.course_instructor_panel_members[i].course.course_type
            _instructor['course_credit'] = info.course_instructor_panel_members[i].course.theory_credit;
            _instructor['see'] = info.course_instructor_panel_members[i].course.see;
            _instructor['last_date'] = date
            instructor_list.push(_instructor);
            _instructor = {}
            _instructor['role'] = 'external'
            _instructor['uuid'] = info.course_instructor_panel_members[i].uuid
            _instructor['uuid_expire_date'] = info.course_instructor_panel_members[i].uuid_expire_date
            _instructor['name'] = info.course_instructor_panel_members[i].external_instructor.name;
            _instructor['email'] = info.course_instructor_panel_members[i].external_instructor.email;
            _instructor['department_name'] = info.course_instructor_panel_members[i].course.department_name;
            _instructor['course_code'] = info.course_instructor_panel_members[i].course.course_code;
            _instructor['session'] = info.course_instructor_panel_members[i].course.session_name;
            _instructor['course_title'] = info.course_instructor_panel_members[i].course.course_title;
            _instructor['course_type'] = info.course_instructor_panel_members[i].course.course_type
            _instructor['course_credit'] = info.course_instructor_panel_members[i].course.theory_credit;
            _instructor['see'] = info.course_instructor_panel_members[i].course.see;
            _instructor['last_date'] = date
            instructor_list.push(_instructor)
        }

        const sendMail = require('../../services/emailService')

        for (let i = 0; i < instructor_list.length; i++) {
            let data = {
                    type: instructor_list[i].role,
                    year: info.year,
                    semester: info.semester,
                    session: instructor_list[i].session,
                    department: instructor_list[i].department_name,
                    course_title: instructor_list[i].course_title,
                    course_code: instructor_list[i].course_code,
                    course_credit: instructor_list[i].course_credit,
                    full_marks: instructor_list[i].see

                }
                //console.log(data)
            sendMail({
                from: 'samiuljust2018@gmail',
                to: instructor_list[i].email,
                subject: 'Link share for Create Question',

                html: require('../../services/emailTemplate')({
                    emailFrom: "samiuljust2018@gmail",
                    text: `Please Create A Questiion Paper for Dept: ${instructor_list[i].department_name} Course:${instructor_list[i].course_title}
                        Code: ${instructor_list[i].course_code} Course Type: ${instructor_list[i].course_type} Credit:${instructor_list[i].course_credit}
                        Session: ${instructor_list[i].session} Total Marks: ${instructor_list[i].see}
                        `,
                    //link: `http://localhost:1000/teacher/create-question/${instructor_list[i].role}/${instructor_list[i].uuid}?year=${data.year}&&semester=${data.semester}&&session=${data.session}&&department_name=${data.department}&&course_title=${data.course_title}&&course_code=${data.course_code}&&course_credit=${data.course_credit}&&full_marks=${data.full_marks}`,
                    link: `http://localhost:1000/teacher/create-question/${instructor_list[i].role}/${instructor_list[i].uuid}/${course_instructor_panel_id}`,
                    expires: instructor_list[i].last_date
                })
            })
        }

        //console.log(date)
        return res.json({
            msg: 'email send success'
        })

    } catch (e) {
        console.log(e)
        return res.json({
            msg: 'something wrong'
        })
    }
}



/** 
 * Get Internal List
 * @version 1-11-22
 * @since 1-11-22
 * @param
 * $GET=[$course_examiner_panel_member_id]
*/

exports.getInternalExaminerList=async(req,res,next)=>{
    try{
        const committee_id=req.params.committee_id;
        const exam_committee=await Exam_Committees.findByPk(committee_id,{
            include: [{
                model: Exam_Committee_Members,
                where: { status: '1' },
                include: [{ model: Users },{ model: Exam_Committee_Roles }]
            }]
        })
        const course_examiner_panel_member_id=req.params.id
        const data=await Course_Examiner_Panel_Member.findByPk(course_examiner_panel_member_id,{
            include:[
                {
                    model:Users, as: 'internal_examiner',
                },
                {
                    model:Course
                }
            ]
        })
        console.log("data length",data)
        return res.render('pages/admin/internalExaminerList.ejs', { data, user: req.user, title: 'Internal Examiner List',exam_committee,flashMessage:Flash.getMessage(req) })
    }catch(e){
        console.log(e)
    }
}

/** 
 * Get External List
 * @version 1-11-22
 * @since 1-11-22
 * @param
 * $GET=[$course_examiner_panel_member_id]
*/

exports.getExternalExaminerList=async(req,res,next)=>{
    try{
        const committee_id=req.params.committee_id;
        const exam_committee=await Exam_Committees.findByPk(committee_id,{
            include: [{
                model: Exam_Committee_Members,
                where: { status: '1' },
                include: [{ model: Users },{ model: Exam_Committee_Roles }]
            }]
        })
        const course_examiner_panel_member_id=req.params.id
        const data=await Course_Examiner_Panel_Member.findByPk(course_examiner_panel_member_id,{
            include:[
                {
                    model:Users, as: 'external_examiner',
                },
                {
                    model:Course
                }
            ]
        })
        console.log("data length",data)
        return res.render('pages/admin/externalExaminerList.ejs', { data, user: req.user, title: 'External Examiner List',exam_committee,flashMessage:Flash.getMessage(req) })
    }catch(e){
        console.log(e)
    }
}

/** 
 * Get Third List
 * @version 1-11-22
 * @since 1-11-22
 * @param
 * $GET=[$course_examiner_panel_member_id]
*/

exports.getThirdExaminerList=async(req,res,next)=>{
    try{
        const committee_id=req.params.committee_id;
        const exam_committee=await Exam_Committees.findByPk(committee_id,{
            include: [{
                model: Exam_Committee_Members,
                where: { status: '1' },
                include: [{ model: Users },{ model: Exam_Committee_Roles }]
            }]
        })
        const course_examiner_panel_member_id=req.params.id
        const data=await Course_Examiner_Panel_Member.findByPk(course_examiner_panel_member_id,{
            include:[
                {
                    model:Users, as: 'third_examiner',
                },
                {
                    model:Course
                }
            ]
        })
        console.log("data length",data)
        return res.render('pages/admin/thirdExaminerList.ejs', { data, user: req.user, title: 'Third Examiner List',exam_committee,flashMessage:Flash.getMessage(req) })
    }catch(e){
        console.log(e)
    }
}



/** 
 * Send Email To Internal
 * @version 11-1-22
 * @since 11-1-22
 * @param
 * $GET=[$course_examiner_panel_id, $internal_id]
*/

exports.emailSendControllerToInternal = async(req, res, next) => {
    try {
        var committee_id=req.params.committee_id;
        var course_examiner_panel__member_id = req.params.id
        let date = new Date(req.body.lastDate);

        const data = await Course_Examiner_Panel_Member.findAll({
            where: {
                id: course_examiner_panel__member_id
            }
        })

        let course_examiner_panel_member = []
        let examiner;
        for (let i = 0; i < data.length; i++) {
            examiner = {}
            let uuid = uuid4();
            examiner['id'] = data[i].id,
                examiner['course_examiner_panel_id'] = data[i].course_examiner_panel_id,
                examiner['course_id'] = data[i].course_id,
                examiner['internal'] = data[i].internal,
                examiner['external'] = data[i].external,
                examiner['third'] = data[i].third,
                examiner['uuid'] = uuid,
                examiner['uuid_expire_date'] = date,
                examiner['status'] = '1',
                course_examiner_panel_member.push(examiner)
        }
        console.log(course_examiner_panel_member)
        await Course_Examiner_Panel_Member.bulkCreate(course_examiner_panel_member, {
            updateOnDuplicate: ['course_id', 'internal', 'external', 'third','uuid', 'uuid_expire_date'],
            returning: true
        })
        const course_examiner_panel_id=data[0].course_examiner_panel_id
        const info = await Course_Examiner_Panel.findByPk(course_examiner_panel_id, {
            include: [{
                model: Course_Examiner_Panel_Member,
                where: { status: '1' },
                include: [{ model: Users, as: 'internal_examiner' }, { model: Users, as: 'external_examiner' },{ model: Users, as: 'third_examiner' }, { model: Course }]
            }]
        })
        let course_id=info.course_examiner_panel_members[0].course_id

        let course=await Course.findAll({
            where:{
                id:course_id,
            }
        })
        let course_code=course[0].course_code
        let question_information=await Question_Information.findAll({
            where:{
                course_code:course_code
            }
        })

        if(question_information.length===0){
            return req.flash('fail','Question is not found')
            return res.redirect(`/admin/internal-examiner/list/${committee_id}/${course_examiner_panel__member_id}`);
        }
        else{
            let question_id=question_information[0].id
            console.log('question information id',question_id);
            let examiner_list = [];
            let _examiner;
            
    
            for (let i = 0; i < info.course_examiner_panel_members.length; i++) {
                _examiner = {}
                _examiner['role'] = 'internal'
                _examiner['uuid'] = info.course_examiner_panel_members[i].uuid
                _examiner['uuid_expire_date'] = info.course_examiner_panel_members[i].uuid_expire_date
                _examiner['name'] = info.course_examiner_panel_members[i].internal_examiner.name;
                _examiner['email'] = info.course_examiner_panel_members[i].internal_examiner.email;
                _examiner['department_name'] = info.course_examiner_panel_members[i].course.department_name;
                _examiner['course_code'] = info.course_examiner_panel_members[i].course.course_code;
                _examiner['session'] = info.course_examiner_panel_members[i].course.session_name;
                _examiner['course_title'] = info.course_examiner_panel_members[i].course.course_title;
                _examiner['course_type'] = info.course_examiner_panel_members[i].course.course_type
                _examiner['course_credit'] = info.course_examiner_panel_members[i].course.theory_credit;
                _examiner['see'] = info.course_examiner_panel_members[i].course.see;
                _examiner['last_date'] = date
                examiner_list.push(_examiner);
            }
    
            const sendMail = require('../../services/emailService')
    
            for (let i = 0; i < examiner_list.length; i++) {
                let data = {
                        type: examiner_list[i].role,
                        year: info.year,
                        semester: info.semester,
                        session: examiner_list[i].session,
                        department: examiner_list[i].department_name,
                        course_title: examiner_list[i].course_title,
                        course_code: examiner_list[i].course_code,
                        course_credit: examiner_list[i].course_credit,
                        full_marks: examiner_list[i].see
    
                    }
                    //console.log(data)
                sendMail({
                    from: 'samiuljust2018@gmail',
                    to: examiner_list[i].email,
                    subject: 'Link share for Submit Exam Marks',
    
                    html: require('../../services/emailTemplateForMark')({
                        emailFrom: "samiuljust2018@gmail",
                        text: `Please Submit Exam Marks for Dept: ${examiner_list[i].department_name} Course:${examiner_list[i].course_title}
                            Code: ${examiner_list[i].course_code} Course Type: ${examiner_list[i].course_type} Credit:${examiner_list[i].course_credit}
                            Session: ${examiner_list[i].session} Total Marks: ${examiner_list[i].see}
                            `,
                        //link: `http://localhost:1000/teacher/create-question/${examiner_list[i].role}/${examiner_list[i].uuid}?year=${data.year}&&semester=${data.semester}&&session=${data.session}&&department_name=${data.department}&&course_title=${data.course_title}&&course_code=${data.course_code}&&course_credit=${data.course_credit}&&full_marks=${data.full_marks}`,
                        link: `http://localhost:1000/teacher/submit-marks/${examiner_list[i].role}/${examiner_list[i].uuid}/${course_examiner_panel_id}/${question_id}`,
                        expires: examiner_list[i].last_date
                    })
                })
            }
    
            req.flash('success','Email is Send to Examiner');
            return res.redirect(`/admin/view-exam-committee`);
        }

    } catch (e) {
        console.log(e)
        req.flash('fail','Email is not Send to Examiner');
        return res.redirect(`/admin/view-exam-committee`);
    }
}

exports.emailSendControllerToExternal = async(req, res, next) => {
    try {
        var committee_id=req.params.committee_id;
        var course_examiner_panel_member_id = req.params.id
        let date = new Date(req.body.lastDate);

        const data = await Course_Examiner_Panel_Member.findAll({
            where: {
                id: course_examiner_panel_member_id
            }
        })

        let course_examiner_panel_member = []
        let examiner;
        for (let i = 0; i < data.length; i++) {
            examiner = {}
            let uuid = uuid4();
            examiner['id'] = data[i].id,
                examiner['course_examiner_panel_id'] = data[i].course_examiner_panel_member_id,
                examiner['course_id'] = data[i].course_id,
                examiner['internal'] = data[i].internal,
                examiner['external'] = data[i].external,
                examiner['third'] = data[i].third,
                examiner['uuid'] = uuid,
                examiner['uuid_expire_date'] = date,
                examiner['status'] = '1',
                course_examiner_panel_member.push(examiner)
        }

        await Course_Examiner_Panel_Member.bulkCreate(course_examiner_panel_member, {
            updateOnDuplicate: ['course_id', 'internal', 'external', 'third','uuid', 'uuid_expire_date'],
            returning: true
        })

        const course_examiner_panel_id=data[0].course_examiner_panel_id
        const info = await Course_Examiner_Panel.findByPk(course_examiner_panel_id, {
            include: [{
                model: Course_Examiner_Panel_Member,
                where: { status: '1' },
                include: [{ model: Users, as: 'internal_examiner' }, { model: Users, as: 'external_examiner' },{ model: Users, as: 'third_examiner' }, { model: Course }]
            }]
        })
      
        let course_id=info.course_examiner_panel_members[0].course_id

        let course=await Course.findAll({
            where:{
                id:course_id,
            }
        })
        let course_code=course[0].course_code
        let question_information=await Question_Information.findAll({
            where:{
                course_code:course_code
            }
        })
 
        // let question_information_id=4
        if(question_information.length===0){
             req.flash('fail','Question is not found')
            return res.redirect(`/admin/external-examiner/list/${committee_id}/${course_examiner_panel_member_id}`);
        }
        else{
            let question_id=question_information[0].id
            console.log('question information id',question_id);
            let examiner_list = [];
            let _examiner;
            
    
            for (let i = 0; i < info.course_examiner_panel_members.length; i++) {
                _examiner = {}
                _examiner['role'] = 'external'
                _examiner['uuid'] = info.course_examiner_panel_members[i].uuid
                _examiner['uuid_expire_date'] = info.course_examiner_panel_members[i].uuid_expire_date
                _examiner['name'] = info.course_examiner_panel_members[i].external_examiner.name;
                _examiner['email'] = info.course_examiner_panel_members[i].external_examiner.email;
                _examiner['department_name'] = info.course_examiner_panel_members[i].course.department_name;
                _examiner['course_code'] = info.course_examiner_panel_members[i].course.course_code;
                _examiner['session'] = info.course_examiner_panel_members[i].course.session_name;
                _examiner['course_title'] = info.course_examiner_panel_members[i].course.course_title;
                _examiner['course_type'] = info.course_examiner_panel_members[i].course.course_type
                _examiner['course_credit'] = info.course_examiner_panel_members[i].course.theory_credit;
                _examiner['see'] = info.course_examiner_panel_members[i].course.see;
                _examiner['last_date'] = date
                examiner_list.push(_examiner)
            }
    
            const sendMail = require('../../services/emailService')
    
            for (let i = 0; i < examiner_list.length; i++) {
                let data = {
                        type: examiner_list[i].role,
                        year: info.year,
                        semester: info.semester,
                        session: examiner_list[i].session,
                        department: examiner_list[i].department_name,
                        course_title: examiner_list[i].course_title,
                        course_code: examiner_list[i].course_code,
                        course_credit: examiner_list[i].course_credit,
                        full_marks: examiner_list[i].see
    
                    }
                    //console.log(data)
                sendMail({
                    from: 'samiuljust2018@gmail',
                    to: examiner_list[i].email,
                    subject: 'Link share for Submit Exam Marks',
    
                    html: require('../../services/emailTemplateForMark')({
                        emailFrom: "samiuljust2018@gmail",
                        text: `Please Submit Exam Marks for Dept: ${examiner_list[i].department_name} Course:${examiner_list[i].course_title}
                            Code: ${examiner_list[i].course_code} Course Type: ${examiner_list[i].course_type} Credit:${examiner_list[i].course_credit}
                            Session: ${examiner_list[i].session} Total Marks: ${examiner_list[i].see}
                            `,
                        //link: `http://localhost:1000/teacher/create-question/${examiner_list[i].role}/${examiner_list[i].uuid}?year=${data.year}&&semester=${data.semester}&&session=${data.session}&&department_name=${data.department}&&course_title=${data.course_title}&&course_code=${data.course_code}&&course_credit=${data.course_credit}&&full_marks=${data.full_marks}`,
                        link: `http://localhost:1000/teacher/submit-marks/${examiner_list[i].role}/${examiner_list[i].uuid}/${course_examiner_panel_id}/${question_id}`,
                        expires: examiner_list[i].last_date
                    })
                })
            }
    
            req.flash('success','Email is Send to Examiner');
            return res.redirect(`/admin/view-exam-committee`);
        }

    } catch (e) {
        console.log(e)
        req.flash('fail','Email is not Send to Examiner');
        return res.redirect(`/admin/view-exam-committee`);
    }
}

exports.emailSendControllerToThird = async(req, res, next) => {
    try {
        var committee_id=req.params.committee_id;
        var course_examiner_panel_member_id = req.params.id
        let date = new Date(req.body.lastDate);

        const data = await Course_Examiner_Panel_Member.findAll({
            where: {
                id: course_examiner_panel_member_id
            }
        })

        let course_examiner_panel_member = []
        let examiner;
        for (let i = 0; i < data.length; i++) {
            examiner = {}
            let uuid = uuid4();
            examiner['id'] = data[i].id,
                examiner['course_examiner_panel_id'] = data[i].course_examiner_panel_id,
                examiner['course_id'] = data[i].course_id,
                examiner['internal'] = data[i].internal,
                examiner['external'] = data[i].external,
                examiner['third'] = data[i].third,
                examiner['uuid'] = uuid,
                examiner['uuid_expire_date'] = date,
                examiner['status'] = '1',
                course_examiner_panel_member.push(examiner)
        }

        await Course_Examiner_Panel_Member.bulkCreate(course_examiner_panel_member, {
            updateOnDuplicate: ['course_id', 'internal', 'external', 'third','uuid', 'uuid_expire_date'],
            returning: true
        })

        const course_examiner_panel_id=data[0].course_examiner_panel_id
        const info = await Course_Examiner_Panel.findByPk(course_examiner_panel_id, {
            include: [{
                model: Course_Examiner_Panel_Member,
                where: { status: '1' },
                include: [{ model: Users, as: 'internal_examiner' }, { model: Users, as: 'external_examiner' },{ model: Users, as: 'third_examiner' }, { model: Course }]
            }]
        })
        let course_id=info.course_examiner_panel_members[0].course_id

        let course=await Course.findAll({
            where:{
                id:course_id,
            }
        })
        let course_code=course[0].course_code
        let question_information=await Question_Information.findAll({
            where:{
                course_code:course_code
            }
        })
    
        if(question_information.length===0){
            req.flash('fail','Question is not found')
            return res.redirect(`/admin/third-examiner/list/${committee_id}/${course_examiner_panel_member_id}`);
        }
        else{
            let question_id=question_information[0].id
            console.log('question information id',question_id);
            let examiner_list = [];
            let _examiner;
            
    
            for (let i = 0; i < info.course_examiner_panel_members.length; i++) {
                _examiner = {}
                _examiner['role'] = 'third'
                _examiner['uuid'] = info.course_examiner_panel_members[i].uuid
                _examiner['uuid_expire_date'] = info.course_examiner_panel_members[i].uuid_expire_date
                _examiner['name'] = info.course_examiner_panel_members[i].third_examiner.name;
                _examiner['email'] = info.course_examiner_panel_members[i].third_examiner.email;
                _examiner['department_name'] = info.course_examiner_panel_members[i].course.department_name;
                _examiner['course_code'] = info.course_examiner_panel_members[i].course.course_code;
                _examiner['session'] = info.course_examiner_panel_members[i].course.session_name;
                _examiner['course_title'] = info.course_examiner_panel_members[i].course.course_title;
                _examiner['course_type'] = info.course_examiner_panel_members[i].course.course_type
                _examiner['course_credit'] = info.course_examiner_panel_members[i].course.theory_credit;
                _examiner['see'] = info.course_examiner_panel_members[i].course.see;
                _examiner['last_date'] = date
                examiner_list.push(_examiner)
            }
    
            const sendMail = require('../../services/emailService')
    
            for (let i = 0; i < examiner_list.length; i++) {
                let data = {
                        type: examiner_list[i].role,
                        year: info.year,
                        semester: info.semester,
                        session: examiner_list[i].session,
                        department: examiner_list[i].department_name,
                        course_title: examiner_list[i].course_title,
                        course_code: examiner_list[i].course_code,
                        course_credit: examiner_list[i].course_credit,
                        full_marks: examiner_list[i].see
    
                    }
                    //console.log(data)
                sendMail({
                    from: 'samiuljust2018@gmail',
                    to: examiner_list[i].email,
                    subject: 'Link share for Submit Exam Marks',
    
                    html: require('../../services/emailTemplateForMark')({
                        emailFrom: "samiuljust2018@gmail",
                        text: `Please Submit Exam Marks for Dept: ${examiner_list[i].department_name} Course:${examiner_list[i].course_title}
                            Code: ${examiner_list[i].course_code} Course Type: ${examiner_list[i].course_type} Credit:${examiner_list[i].course_credit}
                            Session: ${examiner_list[i].session} Total Marks: ${examiner_list[i].see}
                            `,
                        //link: `http://localhost:1000/teacher/create-question/${examiner_list[i].role}/${examiner_list[i].uuid}?year=${data.year}&&semester=${data.semester}&&session=${data.session}&&department_name=${data.department}&&course_title=${data.course_title}&&course_code=${data.course_code}&&course_credit=${data.course_credit}&&full_marks=${data.full_marks}`,
                        link: `http://localhost:1000/teacher/submit-marks/${examiner_list[i].role}/${examiner_list[i].uuid}/${course_examiner_panel_id}/${question_id}`,
                        expires: examiner_list[i].last_date
                    })
                })
            }

            req.flash('success','Email is Send to Examiner');
            return res.redirect(`/admin/view-exam-committee`);
        }

    } catch (e) {
        console.log(e)
        req.flash('fail','Email is not Send to Examiner');
        return res.redirect(`/admin/view-exam-committee`);
    }
}

