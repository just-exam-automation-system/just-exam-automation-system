const passport = require('passport')
const Flash=require('../utils/Flash')


exports.getLogin = async(req, res, next) => {
    return res.render('pages/auth/login', { title: "Login Page", user: req.user,flashMessage:Flash.getMessage(req) })
}


exports.googleRedirect = async(req, res, next) => {
    req.flash('success','Logged In Successfully')
    //res.send(req.user)
    return res.redirect('/')
}


exports.getLogout = async(req, res, next) => {

    req.flash('success','Logged Out Successfully')
    req.logout()
    return res.redirect('/auth/login')
}