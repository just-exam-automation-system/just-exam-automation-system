const db = require('../../model/database')
const Question_Informations = db.question_informations;
const Questions = db.questions;
const Exam_Committee_Roles = db.exam_committee_roles;
const Users = db.users;
const Course = db.courses;
const Exam_Committee_Members = db.exam_committee_members;
const Exam_Committees = db.exam_committee;
const Course_Instructor_Panel = db.course_instructor_panel;
const Course_Instructor_Panel_Member = db.course_instructor_panel_member;
const Course_Examiner_Panel = db.course_examiner_panel;
const Course_Examiner_Panel_Member = db.course_examiner_panel_member;
const Result = db.results;
const Student = db.students;
const { Op } = require("sequelize");
const Flash = require('../../utils/Flash')

// view selected exam committee
exports.getViewSelectedExamCommittee = async(req, res, next) => {
    try {
        // identify if a teacher is assigned for specific exam committee
        let teacher_id = req.user.id
        const selected_exam_committee = await Exam_Committees.findAll({
                include: [{
                    model: Exam_Committee_Members,
                    where: { status: '1', member_id: teacher_id },
                    include: [{ model: Users }, { model: Exam_Committee_Roles }]
                }]
            })
            //res.json(data)
        return res.render('pages/teacher/viewSelectedExamCommittee', { title: "Homepage-Teacher", user: req.user, selected_exam_committee, flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }
}

// view selected exam committee details
exports.getViewSelectedExamCommitteeDetails = async(req, res, next) => {
    try {
        // identify if a teacher is assigned for specific exam committee
        let exam_committee_id = req.params.id
        const selected_exam_committee_details = await Exam_Committees.findByPk(exam_committee_id, {
                include: [{
                    model: Exam_Committee_Members,
                    where: { status: '1' },
                    include: [{ model: Users }, { model: Exam_Committee_Roles }]
                }]
            })
            //res.json(data)
        return res.render('pages/teacher/viewSelectedExamCommitteeDetails', { title: "Homepage-Teacher", user: req.user, selected_exam_committee_details, flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
    }
}



exports.addQuestionPage = async(req, res, next) => {
    return res.render('pages/teacher/add-question.ejs', { title: 'Add Question', user: req.user, flashMessage: Flash.getMessage(req) })
}

exports.createQuestionPage = async(req, res, next) => {

    try {
        let type = req.params.type
        let uuid = req.params.uuid
        let course_instructor_panel_id = req.params.id

        const info = await Course_Instructor_Panel.findByPk(course_instructor_panel_id, {
            include: [{
                model: Course_Instructor_Panel_Member,
                where: { status: '1', uuid: uuid },
                include: [{ model: Users, as: 'internal_instructor' }, { model: Users, as: 'external_instructor' }, { model: Course }]
            }]
        })

        let questions_info = await Question_Informations.findAll({
                where: {
                    question_type: type,
                    uuid: uuid
                },
                include: [{
                    model: Questions
                }]
            })
            //console.log(questions)
        let questions = [];
        if (questions_info.length != 0) {
            questions = questions_info[0].questions
        }

        //return res.json(questions)
        return res.render('pages/teacher/create-question.ejs', { title: 'Create Question', user: req.user, info, type, questions, flashMessage: Flash.getMessage(req) })
    } catch (e) {
        console.log(e)
        next()
    }
}

exports.postCreateQuestionPage = async(req, res, next) => {

    try {


        let type = req.params.type
        let uuid = req.params.uuid
        let course_instructor_panel_id = req.params.id

        // ......................................................................

        let questions_info = await Question_Informations.findAll({
                where: {
                    question_type: type,
                    uuid: uuid
                },
                include: [{
                    model: Questions
                }]
            })
            //console.log(questions)

        if (questions_info.length > 0) {

            //return res.json(req.body)
            //return res.json(questions_info[0].questions[0].question_information_id)
            let question_array = []
            let current_question;
            for (let i = 0; i < req.body.qstn_number.length; i++) {
                current_question = {}
                current_question['id'] = questions_info[0].questions[i].id
                current_question['question_information_id'] = questions_info[0].questions[0].question_information_id
                current_question['question_number'] = req.body.qstn_number[i]
                current_question['question_section'] = req.body.qstn_section[i]
                current_question['question'] = req.body.qstn[i]
                current_question['question_marks'] = req.body.qstn_marks[i]
                question_array.push(current_question)
            }

            //return res.json(question_array)

            await Questions.bulkCreate(question_array, { updateOnDuplicate: ['question', 'question_marks'], returning: true })

            return res.json({
                msg: 'Question Update Success'
            })

        }

        // ......................................................................



        const info = await Course_Instructor_Panel.findByPk(course_instructor_panel_id, {
            include: [{
                model: Course_Instructor_Panel_Member,
                where: { status: '1', uuid: uuid },
                include: [{ model: Users, as: 'internal_instructor' }, { model: Users, as: 'external_instructor' }, { model: Course }]
            }]
        })



        let course_instructor_panel = course_instructor_panel_id
        let question_type = type
        let year = info.year
        let semester = info.semester
        let session = info.session
        let department = info.department_name
        let course_id = info.course_instructor_panel_members[0].course.id
        let course_title = info.course_instructor_panel_members[0].course.course_title
        let course_code = info.course_instructor_panel_members[0].course.course_code
        let course_credit = info.course_instructor_panel_members[0].course.theory_credit
        let full_marks = info.course_instructor_panel_members[0].course.see


        //console.log(question_array)

        const create_question_information = await Question_Informations.create({
            course_instructor_panel_id: course_instructor_panel,
            question_type: question_type,
            uuid,
            year: year,
            semester: semester,
            session: session,
            department: department,
            course_id: course_id,
            course_title: course_title,
            course_code: course_code,
            course_credit: course_credit,
            full_marks: full_marks
        })
        if (create_question_information) {
            let question_array = []
            let current_question;
            for (let i = 0; i < req.body.qstn_number.length; i++) {
                current_question = {}
                current_question['question_information_id'] = create_question_information.id
                current_question['question_number'] = req.body.qstn_number[i]
                current_question['question_section'] = req.body.qstn_section[i]
                current_question['question'] = req.body.qstn[i]
                current_question['question_marks'] = req.body.qstn_marks[i]
                question_array.push(current_question)
            }
            //const create_question = await Questions.bulkCreate(question_array, {updateOnDuplicate: ['questions', 'question_marks'], returning: true })
            const create_question = await Questions.bulkCreate(question_array, { returning: true })
            if (!create_question) {
                return res.json({ msg: 'question create fail' })
            }
        } else {
            return res.json({ msg: 'question create fail' })
        }
        console.log('question create success')
            //return res.redirect('/teacher/add-question')
        return res.json('Qstn create success')
    } catch (e) {
        console.log(e)
    }
}

/**
 * 
 *Get Submit Mark Page
 * @version 7-10-22
 * @since 7-10-22
 * @param 
 * $GET= [$type, $uuid,$course_examiner_panel_id,$question_inforation_id]
 * 
 */

exports.getSubmitMarkPage = async(req, res, next) => {

    try {
        let type = req.params.type
        let uuid = req.params.uuid
        let course_examiner_panel_id = req.params.id
        let question_information_id = req.params.question_id

        const result = await Result.findAll({
            where: {
                question_information_id: question_information_id
            }
        })

        const info = await Course_Examiner_Panel.findByPk(course_examiner_panel_id, {
            include: [{
                model: Course_Examiner_Panel_Member,
                where: { status: '1', uuid: uuid },
                include: [{ model: Users, as: 'internal_examiner' }, { model: Users, as: 'external_examiner' }, { model: Users, as: 'external_examiner' }, { model: Course }]
            }]
        })

        const question_information = await Question_Informations.findByPk(question_information_id)
        const course_code = question_information.course_code;
        const session = question_information.session;
        const course_id = await Course.findAll({
            where: {
                session_name: session,
                course_code: course_code
            }
        });
        console.log("the course id is",course_id)
        if(type==='internal'){
            if(result.length!==0){
                if(result[0].internal_total!==null){
                    return res.render('pages/teacher/edit-mark.ejs',{title:'Edit Mark',info, user: req.user,result,type,question_information_id,flashMessage:Flash.getMessage(req) });
                }
            }

            else{
                let dept=info.department_name;
                let splited_session=info.session.split("-");
                let roll=splited_session[0].slice(-2);
                let student=await Student.findAll({
                    where:{
                        department_name:dept,
                        course_id:course_id[0].id
                    }
                })
                console.log(student.length);
                let questions_info = await Question_Informations.findAll({
                        where: {
                            question_type: type,
                            uuid: uuid
                        },
                        include: [{
                            model: Questions
                        }]
                    })
                    //console.log(questions)
                let questions = [];
                if (questions_info.length != 0) {
                    questions = questions_info[0].questions
                }

                //return res.json(questions)
                return res.render('pages/teacher/submit-mark.ejs', { title: 'Submit Mark', user: req.user, info, type,question_information_id, questions,student,flashMessage:Flash.getMessage(req) })
            }
        }
        else if(type==='external'){
            if(result.length!==0){
                if(result[0].external_total!==null){
                    return res.render('pages/teacher/edit-mark.ejs',{title:'Edit Mark',info, user: req.user,result,type,question_information_id,flashMessage:Flash.getMessage(req) });
                }
            }
        
            else{
                let dept=info.department_name;
                let splited_session=info.session.split("-");
                let roll=splited_session[0].slice(-2);
                let student=await Student.findAll({
                    where:{
                        department_name:dept,
                        course_id:course_id
                    }
                })
                console.log(student.length);
                let questions_info = await Question_Informations.findAll({
                        where: {
                            question_type: type,
                            uuid: uuid
                        },
                        include: [{
                            model: Questions
                        }]
                    })
                    //console.log(questions)
                let questions = [];
                if (questions_info.length != 0) {
                    questions = questions_info[0].questions
                }

                //return res.json(questions)
                return res.render('pages/teacher/submit-mark.ejs', { title: 'Submit Mark', user: req.user, info, type,question_information_id, questions,student,flashMessage:Flash.getMessage(req) })
            }
        }
        else{
            if(result.length!==0){
                if(result[0].third_total!==null){
                    return res.render('pages/teacher/edit-mark.ejs',{title:'Edit Mark',info, user: req.user,result,type,question_information_id,flashMessage:Flash.getMessage(req) });
                }
            }

            else{
                let dept=info.department_name;
                let splited_session=info.session.split("-");
                let roll=splited_session[0].slice(-2);
                let student=await Student.findAll({
                    where:{
                        department_name:dept,
                        course_id:course_id
                    }
                })
                console.log(student.length);
                let questions_info = await Question_Informations.findAll({
                        where: {
                            question_type: type,
                            uuid: uuid
                        },
                        include: [{
                            model: Questions
                        }]
                    })
                    //console.log(questions)
                let questions = [];
                if (questions_info.length != 0) {
                    questions = questions_info[0].questions
                }

                //return res.json(questions)
                return res.render('pages/teacher/submit-mark.ejs', { title: 'Submit Mark', user: req.user, info, type, question_information_id, questions, student, flashMessage: Flash.getMessage(req) })
            }
        }
    } catch (e) {
        console.log(e)
        next()
    }
}


/**
 *POST Submit Mark Page
 * @version 28-10-22
 * @since 17-10-22
 * @param
 * $POST= [$type,$uuid,$course_examiner_panel_id,$question_inforation_id]
 */

exports.postSubmitMarkPage = async(req, res, next) => {

    try {
        let type = req.params.type

        if (type === 'internal') {
            let uuid = req.params.uuid
            let course_examiner_panel_id = req.params.id
            let question_id = req.params.question_id
            const result = await Result.findAll({
                where: {
                    question_information_id: question_id
                }
            })

            if (result.length === 0) {
                let marks = [];
                let mark = {};

                for (let i = 0; i < req.body.student_id.length; i++) {
                    mark['question_information_id'] = question_id
                    mark['course_examiner_panel_id'] = course_examiner_panel_id
                    mark['student_id'] = req.body.student_id[i]
                    mark['internal_total'] = req.body.internal_total[i]
                    mark['internal_detail'] = req.body.internal_total[i]
                    mark['ce'] = req.body.ce[i]
                    mark['attendence'] = req.body.attendence[i]
                    marks.push(mark)
                    mark = {}
                }

                await Result.bulkCreate(marks)
                    .then(() => {
                        req.flash('success', 'Mark Added Successfully')
                        return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`)
                    })
                    .catch(() => {
                        req.flash('fail', 'Mark cannot be added')
                        return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`)
                    })

            } else {
                let mark_id = []
                for (let i = 0; i < result.length; i++) {
                    mark_id.push(result[i].id)
                }
                let marks = [];
                let mark = {};

                for (let i = 0; i < req.body.student_id.length; i++) {
                    mark['id'] = mark_id[i]
                    mark['question_information_id'] = question_id
                    mark['course_examiner_panel_id'] = course_examiner_panel_id
                    mark['student_id'] = req.body.student_id[i]
                    mark['internal_total'] = req.body.internal_total[i]
                    mark['internal_detail'] = req.body.internal_total[i]
                    mark['ce'] = req.body.ce[i]
                    mark['attendence'] = req.body.attendence[i]

                    marks.push(mark)
                    mark = {}
                }

                await Result.bulkCreate(marks, {
                        updateOnDuplicate: ['question_information_id', 'course_examiner_panel_id', 'student_id', 'internal_total', 'internal_detail', 'ce', 'attendence'],
                        returning: true
                    }, {
                        where: {
                            id: mark_id
                        }
                    })
                    .then(() => {
                        req.flash('success', 'Mark Added Successfully')
                        return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`)
                    }).catch(() => {
                        req.flash('fail', 'Mark cannot be added')
                        return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`)
                    })

            }

        } else if (type === 'external') {
            let uuid = req.params.uuid
            let course_examiner_panel_id = req.params.id
            let question_id = req.params.question_id
            const result = await Result.findAll({
                where: {
                    question_information_id: question_id
                }
            })

            if (result.length === 0) {
                let marks = [];
                let mark = {};
                console.log("Mark id is null")
                for (let i = 0; i < req.body.student_id.length; i++) {
                    mark['question_information_id'] = question_id
                    mark['course_examiner_panel_id'] = course_examiner_panel_id
                    mark['student_id'] = req.body.student_id[i]
                    mark['external_total'] = req.body.external_total[i]
                    mark['external_detail'] = req.body.external_total[i]
                    marks.push(mark)
                    mark = {}
                }
                await Result.bulkCreate(marks)
                    .then(() => {
                        req.flash('success', 'Mark Added Successfully')
                        return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`);
                    })
                    .catch(() => {
                        req.flash('fail', 'Mark cannot be added')
                        return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`);


                    })

            } else {
                let mark_id = []
                for (let i = 0; i < result.length; i++) {
                    mark_id.push(result[i].id)
                }
                let marks = [];
                let mark = {};

                console.log("Mark id is not null")
                for (let i = 0; i < req.body.student_id.length; i++) {
                    mark['id'] = mark_id[i]
                    mark['question_information_id'] = question_id
                    mark['course_examiner_panel_id'] = course_examiner_panel_id
                    mark['student_id'] = req.body.student_id[i]
                    mark['external_total'] = req.body.external_total[i]
                    mark['external_detail'] = req.body.external_total[i]

                    marks.push(mark)
                    mark = {}
                }
                await Result.bulkCreate(marks, {
                        updateOnDuplicate: ['question_information_id', 'course_examiner_panel_id', 'student_id', 'external_total', 'external_detail'],
                        returning: true
                    }, {
                        where: {
                            id: mark_id
                        }
                    })
                    .then(() => {
                        req.flash('success', 'Mark Added Successfully')
                        return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`)
                    }).catch(() => {
                        req.flash('fail', 'Mark cannot be added')
                        return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`)
                    })

            }

        } else {
            let uuid = req.params.uuid
            let course_examiner_panel_id = req.params.id
            let question_id = req.params.question_id
            const result = await Result.findAll({
                where: {
                    question_information_id: question_id
                }
            })


            if (result.length === 0) {
                let marks = [];
                let mark = {};

                for (let i = 0; i < req.body.student_id.length; i++) {
                    mark['question_information_id'] = question_id
                    mark['course_examiner_panel_id'] = course_examiner_panel_id
                    mark['student_id'] = req.body.student_id[i]
                    mark['third_total'] = req.body.third_total[i]
                    mark['third_detail'] = req.body.third_total[i]
                    marks.push(mark)
                    mark = {}
                }
                await Result.bulkCreate(marks)
                    .then(() => {
                        req.flash('success', 'Mark Added Successfully')
                        return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`)
                    })
                    .catch(() => {
                        req.flash('fail', 'Mark cannot be added')
                        return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`)
                    })

            } else {
                let mark_id = []
                for (let i = 0; i < result.length; i++) {
                    mark_id.push(result[i].id)
                }
                let marks = [];
                let mark = {};

                for (let i = 0; i < req.body.student_id.length; i++) {
                    mark['id'] = mark_id[i]
                    mark['question_information_id'] = question_id
                    mark['course_examiner_panel_id'] = course_examiner_panel_id
                    mark['student_id'] = req.body.student_id[i]
                    mark['third_total'] = req.body.third_total[i]
                    mark['third_detail'] = req.body.third_total[i]

                    marks.push(mark)
                    mark = {}
                }
                await Result.bulkCreate(marks, {
                        updateOnDuplicate: ['question_information_id', 'course_examiner_panel_id', 'student_id', 'third_total', 'third_detail'],
                        returning: true
                    }, {
                        where: {
                            id: mark_id
                        }
                    })
                    .then(() => {
                        req.flash('success', 'Mark Added Successfully')
                        return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`)
                    }).catch(() => {
                        req.flash('fail', 'Mark cannot be added')
                        return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`)
                    })

            }
        }
    } catch (e) {
        console.log(e)
    }
}

exports.postEditMarkPage = async(req, res, next) => {
    try {
        let type = req.params.type
        let uuid = req.params.uuid
        let course_examiner_panel_id = req.params.id
        let question_id = req.params.question_id


        if (type === 'internal') {
            let currentdate = new Date();
            let datetime = currentdate.getFullYear() + "-" +
                (currentdate.getMonth() + 1) + "-" +
                currentdate.getDate() + " " +
                currentdate.getHours() + ":" +
                currentdate.getMinutes() + ":" +
                currentdate.getSeconds();

            //return res.json(datetime);

            let marks = [];
            let mark = {};

            for (let i = 0; i < req.body.student_id.length; i++) {
                mark['id'] = req.body.id[i]
                mark['question_information_id'] = question_id
                mark['course_examiner_panel_id'] = course_examiner_panel_id
                mark['student_id'] = req.body.student_id[i]
                mark['internal_total'] = req.body.internal_total[i]
                mark['internal_detail'] = req.body.internal_total[i]
                mark['ce'] = req.body.ce[i]
                mark['attendence'] = req.body.attendence[i]
                mark['internal_last_edit'] = datetime

                marks.push(mark)
                mark = {}
            }

            await Result.bulkCreate(marks, {
                    updateOnDuplicate: ['question_information_id', 'course_examiner_panel_id', 'student_id', 'internal_total', 'internal_detail', 'ce', 'attendence', 'internal_last_edit'],
                    returning: true
                })
                .then(() => {
                    req.flash('success', 'Mark Updated Successfully')
                    return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`)
                }).catch(() => {
                    req.flash('fail', 'Mark cannot be Updated')
                    return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`)
                })

        } else if (type === 'external') {
            let currentdate = new Date();
            let datetime = currentdate.getFullYear() + "-" +
                (currentdate.getMonth() + 1) + "-" +
                currentdate.getDate() + " " +
                currentdate.getHours() + ":" +
                currentdate.getMinutes() + ":" +
                currentdate.getSeconds();

            const result = await Result.findAll({
                where: {
                    question_information_id: question_id
                }
            })

            let marks = [];
            let mark = {};

            for (let i = 0; i < req.body.student_id.length; i++) {
                mark['id'] = req.body.id[i]
                mark['question_information_id'] = question_id
                mark['course_examiner_panel_id'] = course_examiner_panel_id
                mark['student_id'] = req.body.student_id[i]
                mark['external_total'] = req.body.external_total[i]
                mark['external_detail'] = req.body.external_total[i]
                mark['external_last_edit'] = datetime

                marks.push(mark)
                mark = {}
            }
            await Result.bulkCreate(marks, {
                    updateOnDuplicate: ['question_information_id', 'course_examiner_panel_id', 'student_id', 'external_total', 'external_detail', 'external_last_edit'],
                    returning: true
                })
                .then(() => {
                    req.flash('success', 'Mark Updated Successfully')
                    return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`)
                }).catch(() => {
                    req.flash('fail', 'Mark cannot be Updated')
                    return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`)
                })

        } else {
            let currentdate = new Date();
            let datetime = currentdate.getFullYear() + "-" +
                (currentdate.getMonth() + 1) + "-" +
                currentdate.getDate() + " " +
                currentdate.getHours() + ":" +
                currentdate.getMinutes() + ":" +
                currentdate.getSeconds();

            const result = await Result.findAll({
                where: {
                    question_information_id: question_id
                }
            })

            let marks = [];
            let mark = {};

            for (let i = 0; i < req.body.student_id.length; i++) {
                mark['id'] = req.body.id[i]
                mark['question_information_id'] = question_id
                mark['course_examiner_panel_id'] = course_examiner_panel_id
                mark['student_id'] = req.body.student_id[i]
                mark['third_total'] = req.body.third_total[i]
                mark['third_detail'] = req.body.third_total[i]
                mark['third_last_edit'] = datetime

                marks.push(mark)
                mark = {}
            }
            await Result.bulkCreate(marks, {
                    updateOnDuplicate: ['question_information_id', 'course_examiner_panel_id', 'student_id', 'third_total', 'third_detail', 'third_last_edit'],
                    returning: true
                })
                .then(() => {
                    req.flash('success', 'Mark Updated Successfully')
                    return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`);
                }).catch(() => {
                    req.flash('fail', 'Mark cannot be Updated')
                    return res.redirect(`/teacher/submit-marks/${type}/${uuid}/${course_examiner_panel_id}/${question_id}`);
                })

        }
    } catch (e) {
        console.log(e)
    }
}