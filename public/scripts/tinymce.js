window.onload = function() {
    for (let i = 0; i < 32; i++) {
        let str = 'textarea#tiny-mce-post-body' + i
        tinymce.init({
            selector: str,
            // external_plugins: {
            //     'tiny_mce_wiris': `node_modules/@wiris/mathtype-tinymce5/plugin.min.js`,
            //    },
            external_plugins: { tiny_mce_wiris: 'https://www.wiris.net/demo/plugins/tiny_mce/plugin.js' },
            plugins: ["advlist lists link autolink autosave code",
                'preview', 'searchreplace', 'wordcount', 'media table emoticons image imagetools'
            ],
            toolbar: 'bold italic underline | alignleft alignright aligncenter alignjustify | bullist numlist outdent indent | link image media | forecolor backcolor emoticons | code preview tiny_mce_wiris_formulaEditor tiny_mce_wiris_formulaEditorChemistry',
            //toolbar: 'bold tiny_mce_wiris_formulaEditor tiny_mce_wiris_formulaEditorChemistry',
            height: 180,
            htmlAllowedTags: ['.*'],
            htmlAllowedAttrs: ['.*'],

            draggable_modal: true,
            automatic_uploads: true,
            images_upload_url: '/uploads/questionImage',
            relative_urls: false,
            images_upload_handler: function(blobInfo, success, failure) {
                let headers = new Headers()
                headers.append('Accept', 'Application/JSON')

                let formData = new FormData()
                formData.append('question-image', blobInfo.blob(), blobInfo.filename())

                let req = new Request('/uploads/questionImage', {
                    method: 'POST',
                    headers,
                    mode: 'cors',
                    body: formData

                })

                fetch(req)
                    .then(res => res.json())
                    .then(data => success(data.imageUrl))
                    .catch(() => failure('HTTP Error'))
            }
        })

    }
}