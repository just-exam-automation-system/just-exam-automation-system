const authRoute = require('./authRoute')
const homePageRoute = require('./homePageRoute')
const teacherRoute = require('./teacher/teacherRoute')
const adminRoute = require('./admin/adminRoute')
const uploadRoute = require('./uploadRoute')
const routes = [{
        path: '/uploads',
        handler: uploadRoute
    },
    {
        path: '/teacher',
        handler: teacherRoute
    },
    {
        path: '/admin',
        handler: adminRoute
    },
    {
        path: '/auth',
        handler: authRoute
    },
    {
        path: '/',
        handler: homePageRoute
    },

]

module.exports = (app) => {
    routes.forEach(r => {
        if (r.path == '/') {
            app.get(r.path, r.handler)
        } else {
            app.use(r.path, r.handler)
        }
    })
}