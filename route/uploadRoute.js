const router = require('express').Router()

const upload = require('../middleware/questionImageUpload')
const { questionImageUploadController } = require('../controller/uploadController')

router.post('/questionImage', upload.single('question-image'), questionImageUploadController)

module.exports = router