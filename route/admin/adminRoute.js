const router = require('express').Router()
const {
    getCourseHomePage,
    getCourgeAddFilePageController,
    postCourseFilePage,
    getCoursePage,
    submitCourse,
    viewAllCourses,
    viewAllCoursesDetailsPageInSession,
    getEditSemesterCoursePageController,
    postEditSemesterCoursePageController,
    deleteCourse,
    getCreateExamCommittee,
    getExamCommitteeRoles,
    postCreateExamCommittee,
    getTeachers,
    getExternalTeachers,
    getInternalTeachers,
    viewExamCommitteePage,
    viewExamCommitteePageDetailsPage,
    disableExamCommittee,
    getEditExamCommitteePage,
    postEditExamCommitteePage,
    disableExamCommitteeMember,
    createCourseInstructorPanel,
    createCourseInstructorPanelPostController,
    getAllCourses,
    viewCourseInstructorPanelPage,
    viewCourseInstructorPanelPageDetails,
    disableCourseInstructorPanel,
    getEditCourseInstructorPanelPage,
    postEditCourseInstructorPanelPage,
    disableCourseInstructorPanelMember,
    createCourseExaminerPanel,
    createExaminerPanelPostController,
    getEditCourseExaminerPanelPage,
    postEditCourseExaminerPanelPage,
    disableCourseExaminerPanel,
    getStudentRegisterPage,
    getStudentRegisterPageController,
    postStudentRegisterPageController,
    postSubmitStudentRegisterController,
    getExamineCourses
} = require('../../controller/admin/admin_controller')



const { getAllInstractorList, emailSendController, getInternalExaminerList, getExternalExaminerList, getThirdExaminerList, emailSendControllerToInternal, emailSendControllerToExternal, emailSendControllerToThird } = require('../../controller/admin/email_controller')

// const { getAllInstractorList, emailSendController } = require('../../controller/admin/email_controller')

// const { getQuestionsController } = require('../../controller/admin/question_controller')

const { getQuestionsController, postModerateQuestionsController, getModerateQuestionController, get_all_questions } = require('../../controller/admin/question_controller')
const {
    getAddInternalExternalPage,
    postAddInternalExternalPage,
    getAllInternalExternal,
    getSingleInternalExternal,
    enableDisableInternalExternal,
    updateIinternalExternal
} = require('../../controller/admin/internalExternal_controller')


const upload = require('../../middleware/upload')

router.get('/add-course', getCourseHomePage)
router.get('/add-course-file', getCourgeAddFilePageController)
router.post('/add-course', upload.single('course'), postCourseFilePage)
router.get('/question-details', getCoursePage)
router.post('/course', submitCourse)
router.get('/view-courses', viewAllCourses)
router.get('/view-courses/:id', viewAllCoursesDetailsPageInSession)
router.get('/edit-course/:id/:sem', getEditSemesterCoursePageController)
router.post('/edit-course/:id/:sem', postEditSemesterCoursePageController)
router.get('/delete/:id', deleteCourse)
router.get('/create-exam-committee', getCreateExamCommittee)
router.post('/create-exam-committee', postCreateExamCommittee)

// api get 
router.get('/api/exam-committee-roles', getExamCommitteeRoles)
router.get('/api/get-teachers', getTeachers)
router.get('/api/get-Externalteachers', getExternalTeachers)
router.get('/api/get-Internalteachers', getInternalTeachers)
router.get('/api/:department/:session/:year/:semester/get-courses', getAllCourses)
router.get('/api/:department/:session/:year/:semester/get-question-courses', getExamineCourses);

// // api get 



// router.get('/api/exam-committee-roles', getExamCommitteeRoles)
// router.get('/api/get-teachers', getTeachers)

// view exam committee
router.get('/view-exam-committee', viewExamCommitteePage)
    // view exam committee details
router.get('/view-exam-committee/:id', viewExamCommitteePageDetailsPage)
    // disable exma committee api 
router.put('/view-exam-committee/disable/:id', disableExamCommittee)

// get edit exam committee page 
router.get('/edit-exam-committee/:id', getEditExamCommitteePage)

// post edit exam committee page 
router.post('/edit-exam-committee/:id', postEditExamCommitteePage)

// disable exma committee api 
router.put('/edit-exam-committee/disable-member/:id', disableExamCommitteeMember)

//create course instructor panel
router.get('/create-instructor-panel/:id', createCourseInstructorPanel)
    //create course instructor panel post route
router.post('/create-instructor-panel/:id', createCourseInstructorPanelPostController)
    //view course instructor panel route
router.get('/view-course-instructor-panel', viewCourseInstructorPanelPage)
    //view course instructor panel details route
router.get('/view-course-instructor-panel/:id', viewCourseInstructorPanelPageDetails)
    //disable course instructor panel
router.put('/view-course-instructor-panel/disable/:id', disableCourseInstructorPanel)
    //get edit course instructor panel page
router.get('/edit-course-instructor-panel/:id', getEditCourseInstructorPanelPage)
    //post edit course instructor panel
router.post('/edit-course-instructor-panel/:id', postEditCourseInstructorPanelPage)
router.put('/edit-course-instructor-panel/disable-member/:id', disableCourseInstructorPanelMember)



// get all instractor 
router.get('/course-instractor/all-list/:id', getAllInstractorList)
    // email send 
router.post('/course-instructor/send-email/:id', emailSendController)


//create course examiner panel  route
router.get('/create-examiner-panel/:id', createCourseExaminerPanel)

//create course examiner panel post route
router.post('/create-examiner-panel/:id', createExaminerPanelPostController)

//edit course examiner panel get route
router.get('/edit-course-examiner-panel/:id', getEditCourseExaminerPanelPage)

//edit course examiner panel post route
router.post('/edit-course-examiner-panel/:id', postEditCourseExaminerPanelPage)

//disable examiner panel member
router.put('/view-course-examiner-panel/disable/:id', disableCourseExaminerPanel)

//get all examienr
router.get('/internal-examiner/list/:committee_id/:id',getInternalExaminerList)
router.get('/external-examiner/list/:committee_id/:id',getExternalExaminerList)
router.get('/third-examiner/list/:committee_id/:id',getThirdExaminerList)
router.post('/internal-examiner/send-email/:committee_id/:id', emailSendControllerToInternal)
router.post('/external-examiner/send-email/:committee_id/:id',emailSendControllerToExternal)
router.post('/third-examiner/send-email/:committee_id/:id',emailSendControllerToThird)

// module.exports = router
// get questions 
// questions 


router.get('/question/:type/:id/:uuid', getQuestionsController)
router.get('/question/view/:type/:id/:uuid', getModerateQuestionController)
router.post('/question/:type/:id/:uuid', postModerateQuestionsController)
router.get('/all-question/:type/:id/:uuid', get_all_questions)

// questions



router.get('/add-internal-external', getAddInternalExternalPage)
router.post('/add-internal-external', postAddInternalExternalPage)

router.get('/view-internal-external', getAllInternalExternal)
router.get('/view-internal-external/profile/:id', getSingleInternalExternal)
router.get('/view-internal-external/profile/enable-disable/:id', enableDisableInternalExternal)
router.post('/view-internal-external/profile/update/:id', updateIinternalExternal)

//student resgistration 

router.get('/registration/:id/:course_instructor_panel_member_id/:course_id', getStudentRegisterPage)
router.get('/:id/:course_instructor_panel_member_id/:course_id/add-register-file', getStudentRegisterPageController)
router.post('/:id/:course_instructor_panel_member_id/:course_id/add-register-file', upload.single('student'), postStudentRegisterPageController)
router.post('/:id/:course_instructor_panel_member_id/:course_id/submit-register-students', postSubmitStudentRegisterController);
module.exports = router