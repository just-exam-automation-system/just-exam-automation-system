const router = require('express').Router()
const { getViewSelectedExamCommittee, getViewSelectedExamCommitteeDetails, addQuestionPage, createQuestionPage, postCreateQuestionPage, getSubmitMarkPage, postSubmitMarkPage, postEditMarkPage } = require('../../controller/teacher/teacher_controller')
const { authCheck } = require('../../middleware/authCheck')


router.get('/selected-committee', authCheck, getViewSelectedExamCommittee)
router.get('/view-exam-committee/:id', authCheck, getViewSelectedExamCommitteeDetails)

router.get('/add-question', authCheck, addQuestionPage)
router.get('/create-question/:type/:uuid/:id', createQuestionPage)
router.post('/create-question/:type/:uuid/:id', postCreateQuestionPage)
router.get('/submit-marks/:type/:uuid/:id/:question_id',getSubmitMarkPage)
router.post('/submit-marks/:type/:uuid/:id/:question_id',postSubmitMarkPage)
router.post('/edit-marks/:type/:uuid/:id/:question_id',postEditMarkPage)

module.exports = router